# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from montant import *
from menage import *
from z3 import *
from typing import Union, Optional


class APL2019:
    counter: int = 0

    def __init__(self, menage: Menage):
        self.menage = menage

        self.montant_forfaitaire_charges_: Optional[Montant] = None
        self.depense_logement_plafonnee_: Optional[Montant] = None
        self.participation_minimale_: Optional[Montant] = None
        self.taux_familial_pour_10000_: Optional[BitVec] = None
        self.taux_complementaire_pour_10000_: Optional[BitVec] = None
        self.abattement_forfaitaire_: Optional[Montant] = None
        self.montant_: Optional[Montant] = None
        self.calcul_avec_etapes_: Optional[List[Union[BitVec, Montant]]] = None

    def loyer_plafond(self, s: Solver, zone: Union[int, BitVec]) -> Montant:
        # HYPOTHÈSE: pas de colocation
        # Source: https://www.journaldunet.fr/patrimoine/guide-des-finances-personnelles/1196553-apl-2019-simulation-condition-et-demande-01-04-2019/
        base_isole = Montant.anonyme(s, "loyer_plafond_base_isole")
        s.add(Implies(
            zone == 1,
            base_isole == Montant.centimes(29505, s)
        ))
        s.add(Implies(
            zone == 2,
            base_isole == Montant.centimes(25714, s)
        ))
        s.add(Implies(
            zone == 3,
            base_isole == Montant.centimes(24100, s)
        ))
        base_couple_sans_enfants = Montant.anonyme(s, "loyer_plafond_base_couple_sans_enfants")
        s.add(Implies(
            zone == 1,
            base_couple_sans_enfants == Montant.centimes(35585, s)
        ))
        s.add(Implies(
            zone == 2,
            base_couple_sans_enfants == Montant.centimes(31474, s)
        ))
        s.add(Implies(
            zone == 3,
            base_couple_sans_enfants == Montant.centimes(29216, s)
        ))
        base_couple_1_enfant = Montant.anonyme(s, "loyer_plafond_base_couple_1_enfants")
        s.add(Implies(
            zone == 1,
            base_couple_1_enfant == Montant.centimes(40218, s)
        ))
        s.add(Implies(
            zone == 2,
            base_couple_1_enfant == Montant.centimes(35417, s)
        ))
        s.add(Implies(
            zone == 3,
            base_couple_1_enfant == Montant.centimes(32759, s)
        ))
        base_a_charge = Montant.anonyme(s, "loyer_plafond_base_a_charge")
        s.add(Implies(
            zone == 1,
            base_a_charge == Montant.centimes(5834, s)
        ))
        s.add(Implies(
            zone == 2,
            base_a_charge == Montant.centimes(5154, s)
        ))
        s.add(Implies(
            zone == 3,
            base_a_charge == Montant.centimes(4695, s)
        ))
        npc = self.menage.nb_personnes_a_charge(s)
        npc_minus_1 = Montant.bitvec_sat_sub(npc, 1, s)
        out = Montant.anonyme(s, "loyer_plafond")
        s.add(Implies(
            And(self.menage.composition == Composition.celibataire(), npc == 0),
            out == base_isole
        ))
        s.add(Implies(
            And(Or(self.menage.composition == Composition.couple(),
            self.menage.composition == Composition.concubinage()), npc == 0),
            out == base_couple_sans_enfants
        ))
        s.add(Implies(
            npc > 0,
            out == base_couple_1_enfant + base_a_charge * npc_minus_1
        ))
        return out

    def montant_forfaitaire_charges(self, s: Solver) -> Montant:
        if self.montant_forfaitaire_charges_ is None:
            out = Montant.anonyme(s, "montant_forfaitaire_charges")
            npc = self.menage.nb_personnes_a_charge(s)
            npc_minus_1 = Montant.bitvec_sat_sub(npc, 1, s)
            out = Montant.anonyme(s, "loyer_plafond")
            s.add(Implies(
                npc == 0,
                out == Montant.centimes(5367, s)
            ))
            s.add(Implies(
                npc == 1,
                out == Montant.centimes(6583, s)
            ))
            s.add(Implies(
                npc > 1,
                out == Montant.centimes(6583, s) + Montant.centimes(1216, s) * npc_minus_1
            ))
            self.montant_forfaitaire_charges_ = out
        return self.montant_forfaitaire_charges_

    def depense_logement_plafonnee(self, s: Solver) -> Montant:
        if self.depense_logement_plafonnee_ is None:
            out = Montant.anonyme(s, "depense_logement_plafonnee")
            plafond = self.loyer_plafond(s, self.menage.logement.zone)
            s.add(Implies(
                self.menage.logement.mensualite <= plafond,
                out == self.menage.logement.mensualite
            ))
            s.add(Implies(
                self.menage.logement.mensualite > plafond,
                out == plafond
            ))
            self.depense_logement_plafonnee_ = out
        return self.depense_logement_plafonnee_

    def participation_minimale(self, s: Solver) -> Montant:
        if self.participation_minimale_ is None:
            out = Montant.anonyme(s, "participation_minimale")
            x = (self.depense_logement_plafonnee(s) + self.montant_forfaitaire_charges(s)) % Taux.pourmille(85)
            y = Montant.centimes(3502, s)
            s.add(Implies(x > y, out == x))
            s.add(Implies(x <= y, out == y))
            self.paricipation_minimale_ = out
            return out
        else:
            return self.participation_minimale_

    def taux_familial_pour_10000(self, s: Solver) -> BitVec:
        if self.taux_familial_pour_10000_ is None:
            out = BitVec("taux_familial_pour_10000_{}".format(APL2019.counter), repr_bits)
            APL2019.counter += 1

            npc = self.menage.nb_personnes_a_charge(s)
            npc_minus_3 = Montant.bitvec_sat_sub(npc, 3, s)
            s.add(Implies(
                And(npc == 0, self.menage.composition == Composition.celibataire()),
                out == 283
            ))
            s.add(Implies(
                And(npc == 0, Or(self.menage.composition == Composition.concubinage(),
                self.menage.composition == Composition.couple())),
                out == 315
            ))
            s.add(Implies(
                npc == 1,
                out == 270
            ))
            s.add(Implies(
                npc == 2,
                out == 238
            ))
            s.add(Implies(
                npc == 3,
                out == 201
            ))
            s.add(Implies(
                npc > 3,
                out == 201 - 6 * npc_minus_3
            ))
            self.taux_familial_pour_1000_ = out
            return out
        else:
            return self.taux_familial_pour_10000_

    def taux_complementaire_pour_10000(self, s: Solver) -> BitVec:
        if self.taux_complementaire_pour_10000_ is None:
            out = BitVec("taux_complementaire_pour_10000_{}".format(APL2019.counter), repr_bits)
            APL2019.counter += 1

            plafond = self.loyer_plafond(s, 2)
            retenu = self.depense_logement_plafonnee(s)
            C1 = BitVec("C1_{}".format(APL2019.counter), repr_bits)
            APL2019.counter += 1
            C2 = BitVec("C2_{}".format(APL2019.counter), repr_bits)
            APL2019.counter += 1
            C3 = BitVec("C3_{}".format(APL2019.counter), repr_bits)
            APL2019.counter += 1
            C4 = BitVec("C4_{}".format(APL2019.counter), repr_bits)
            APL2019.counter += 1
            C5 = BitVec("C5_{}".format(APL2019.counter), repr_bits)
            APL2019.counter += 1

            RL = (retenu.valeur * 10000) / plafond.valeur

            s.add(Implies(
                RL >= 7500,
                And(C1 == 10000, C2 == 6800, C3 == 6800, C4 == 3750, C5 == 31)
            ))
            s.add(Implies(
                And(RL >= 4500, RL < 7500),
                And(C1 == RL, C2 == 4500, C3 == (C1 * C2) / 10000, C4 == 2025, C5 == (C3 - C4) / 10000)
            ))
            s.add(Implies(
                RL < 4500,
                And(C1 == RL, C2 == 0, C3 == (C1 * C2) / 10000, C4 == 0, C5 == (C3 - C4) / 10000)
            ))
            s.add(out == C5)
            self.taux_complementaire_pour_10000_ = C5
            return out
        else:
            return self.taux_complementaire_pour_10000_

    def abattement_forfaitaire(self, s: Solver) -> Montant:
        if self.abattement_forfaitaire_ is None:
            out = Montant.anonyme(s, "abattement_forfaitaire")
            npc = self.menage.nb_personnes_a_charge(s)
            npc_minus_6 = Montant.bitvec_sat_sub(npc, 6, s)
            s.add(Implies(
                And(npc == 0, self.menage.composition == Composition.celibataire()),
                out == Montant.euros(4575, s)
            ))
            s.add(Implies(
                And(npc == 0, Or(self.menage.composition == Composition.concubinage(),
                self.menage.composition == Composition.couple())),
                out == Montant.euros(6553, s)
            ))
            s.add(Implies(
                npc == 1,
                out == Montant.euros(7816, s)
            ))
            s.add(Implies(
                npc == 2,
                out == Montant.euros(7992, s)
            ))
            s.add(Implies(
                npc == 3,
                out == Montant.euros(8298, s)
            ))
            s.add(Implies(
                npc == 4,
                out == Montant.euros(8606, s)
            ))
            s.add(Implies(
                npc == 5,
                out == Montant.euros(8912, s)
            ))
            s.add(Implies(
                npc == 6,
                out == Montant.euros(9219, s)
            ))
            s.add(Implies(
                npc > 6,
                out == 9219 + 305 * npc_minus_6
            ))
            self.abattement_forfaitaire_ = out
        return self.abattement_forfaitaire_

    def calcul_avec_etapes(self, s: Solver) -> List[Union[BitVec, Montant]]:
        if self.calcul_avec_etapes_ is None:
            L = self.depense_logement_plafonnee(s)
            C = self.montant_forfaitaire_charges(s)
            Mf0 = Montant.euros(5, s)
            P0 = self.participation_minimale(s)
            TF = self.taux_familial_pour_10000(s)
            TL = self.taux_complementaire_pour_10000(s)
            Tp = TF + TL
            Rp = self.menage.revenu_fiscal_reference(s).sub(self.abattement_forfaitaire(s))
            Pp = P0 + (Rp * Tp) // 10000
            self.calcul_avec_etapes_ = [L, C, P0, Mf0, TF, TL, Tp, Rp, Pp]
        return self.calcul_avec_etapes_

    def montant(self, s: Solver) -> Montant:
        # HYPOTHÈSE: APL2019 uniquement calculée pour les locations
        if self.montant_ is None:
            out = Montant.anonyme(s, "montant_apl")
            [L, C, P0, Mf0, TF, TL, Tp, Rp, Pp] = self.calcul_avec_etapes(s)
            apl = (L + C).sub(Pp).sub(Mf0).round_to_euro()  # type: ignore
            is_given = And(apl < Montant.euros(15, s), self.menage.logement.is_location())
            s.add(Implies(
                is_given,
                out == Montant.euros(0, s)
            ))
            s.add(Implies(
                Not(is_given),
                out == apl
            ))
            self.montant_ = out
        return self.montant_
