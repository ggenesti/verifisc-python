# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from montant import *
from foyerfiscal import *
from menage import *
from typing import Optional, Union, Any, List
from solveur import *


class TrancheIR2020:
    def __init__(self, min: Montant, max: Optional[Montant], t: Taux):
        self.min = min
        self.max = max
        self.taux = t

    @staticmethod
    def tranche1(s: Solver) -> 'TrancheIR2020':
        return TrancheIR2020(
            Montant.euros(0, s),
            Montant.euros(10064, s),
            Taux.pourcent(0)
        )

    @staticmethod
    def tranche2(s: Solver) -> 'TrancheIR2020':
        return TrancheIR2020(
            Montant.euros(10065, s),
            Montant.euros(25659, s),
            Taux.pourcent(11)
        )

    @staticmethod
    def tranche3(s: Solver) -> 'TrancheIR2020':
        return TrancheIR2020(
            Montant.euros(25660, s),
            Montant.euros(73369, s),
            Taux.pourcent(30)
        )

    @staticmethod
    def tranche4(s: Solver) -> 'TrancheIR2020':
        return TrancheIR2020(
            Montant.euros(73370, s),
            Montant.euros(157806, s),
            Taux.pourcent(41)
        )

    @staticmethod
    def tranche5(s: Solver) -> 'TrancheIR2020':
        return TrancheIR2020(
            Montant.euros(157807, s),
            None,
            Taux.pourcent(45)
        )

    def revenu_tranche(self, r: Montant, s: Solver) -> Montant:
        if self.max is None:
            return r.sub(self.min)
        else:
            out = Montant.anonyme(s, "tranche_{}".format(self.taux.num))
            s.add(Implies(r >= self.max, out == self.max.sub(self.min)))
            s.add(Implies(r < self.max, out == r.sub(self.min)))
            return out

    def montant(self, r: Montant, s: Solver) -> Montant:
        return self.revenu_tranche(r, s) % self.taux


class QuotientFamilial2020:
    counter: int = 0

    def __init__(self, parts: Union[int, BitVecVal, BitVec], plafond_q_f: Montant, plafond_num_rscr: Montant, plafond_rscr: Montant):
        self.demi_parts = parts
        self.plafond_q_f = plafond_q_f
        self.plafond_num_rscr = plafond_num_rscr
        self.plafond_rscr = plafond_rscr

    def __add__(self, rhs: 'QuotientFamilial2020') -> 'QuotientFamilial2020':
        return QuotientFamilial2020(
            self.demi_parts + rhs.demi_parts,
            self.plafond_q_f + rhs.plafond_q_f,
            self.plafond_num_rscr + rhs.plafond_num_rscr,
            self.plafond_rscr + rhs.plafond_rscr
        )

    def __eq__(self, rhs: Any) -> Any:
        return And(
            self.demi_parts == rhs.demi_parts,
            self.plafond_q_f == rhs.plafond_q_f,
            self.plafond_num_rscr == rhs.plafond_num_rscr,
            self.plafond_rscr == rhs.plafond_rscr
        )

    def __ne__(self, rhs: Any) -> Any:
        return Or(
            self.demi_parts != rhs.demi_parts,
            self.plafond_q_f != rhs.plafond_q_f,
            self.plafond_num_rscr != rhs.plafond_num_rscr,
            self.plafond_rscr != rhs.plafond_rscr
        )

    @staticmethod
    def variable(name: str, s: Solver) -> 'QuotientFamilial2020':
        q_f = QuotientFamilial2020(
            BitVec(name + "_demi_parts", repr_bits),
            Montant.anonyme(s, name + "_plafond_q_f"),
            Montant.anonyme(s, name + "_plafond_num_rscr"),
            Montant.anonyme(s, name + "_plafond_rscr")
        )
        # HYPOTHÈSE: pas plus de 24 demi-parts pour le quotient familial
        s.add(q_f.demi_parts >= 0, q_f.demi_parts <= 24)
        return q_f

    @staticmethod
    def anonyme(s: Solver) -> 'QuotientFamilial2020':
        out = QuotientFamilial2020.variable("qf{}".format(QuotientFamilial2020.counter), s)
        QuotientFamilial2020.counter += 1
        return out

    @staticmethod
    def diviser(r: Montant, q_f: 'QuotientFamilial2020', s: Solver) -> Montant:
        return (r // q_f.demi_parts) * 2

    @staticmethod
    def multiplier(r: Montant, qf: 'QuotientFamilial2020') -> Montant:
        return (r * qf.demi_parts) // 2

    @staticmethod
    def plafond_enfant_a_charge(s: Solver) -> Montant:
        return Montant.euros(1551, s)

    @staticmethod
    def plafond_enfant_a_charge_parent_isole(s: Solver) -> Montant:
        return Montant.euros(3660, s)

    @staticmethod
    def plafond_enfant_a_charge_invalide(s: Solver) -> Montant:
        return Montant.euros(3098, s)

    @staticmethod
    def plafond_enfant_a_charge_veuf(s: Solver) -> Montant:
        return Montant.euros(4830, s)

    @staticmethod
    def plafond_num_rscr_celibataire(s: Solver) -> Montant:
        return Montant.euros(21037, s)

    @staticmethod
    def plafond_num_rscr_couple(s: Solver) -> Montant:
        return Montant.euros(42074, s)

    @staticmethod
    def plafond_num_rscr_demi_part(s: Solver) -> Montant:
        return Montant.euros(3797, s)

    @staticmethod
    def plafond_rscr_celibataire(s: Solver) -> Montant:
        return Montant.euros(18985, s)

    @staticmethod
    def plafond_rscr_couple(s: Solver) -> Montant:
        return Montant.euros(37970, s)

    @staticmethod
    def plafond_rscr_demi_part(s: Solver) -> Montant:
        return Montant.euros(3737, s)

    @staticmethod
    def celibataire(f: FoyerFiscal, s: Solver) -> 'QuotientFamilial2020':
        qf = QuotientFamilial2020.anonyme(s)

        enfants_minus_2 = Montant.bitvec_sat_sub(f.nb_personnes_a_charge, 2, s)

        # Veuf
        qf_target = QuotientFamilial2020(
            2,
            Montant.euros(0, s),
            QuotientFamilial2020.plafond_num_rscr_celibataire(s),
            QuotientFamilial2020.plafond_rscr_celibataire(s)
        )
        s.add(Implies(And(f.veuf, f.nb_personnes_a_charge == 0), qf == qf_target))
        qf_target += QuotientFamilial2020(
            3,
            QuotientFamilial2020.plafond_enfant_a_charge_veuf(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 3,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 3,
        )
        s.add(Implies(And(f.veuf, f.nb_personnes_a_charge == 1), qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(And(f.veuf, f.nb_personnes_a_charge == 2), qf == qf_target))
        qf_target += QuotientFamilial2020(
            2 * enfants_minus_2,
            QuotientFamilial2020.plafond_enfant_a_charge(s) * enfants_minus_2,
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2 * enfants_minus_2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2 * enfants_minus_2,
        )
        s.add(Implies(And(f.veuf, f.nb_personnes_a_charge >= 3), qf == qf_target))

        # Parent isolé
        qf_target = QuotientFamilial2020(
            2,
            Montant.euros(0, s),
            QuotientFamilial2020.plafond_num_rscr_celibataire(s),
            QuotientFamilial2020.plafond_rscr_celibataire(s)
        )
        s.add(Implies(And(f.parent_isole, f.nb_personnes_a_charge == 0), qf == qf_target))
        qf_target += QuotientFamilial2020(
            2,
            QuotientFamilial2020.plafond_enfant_a_charge_parent_isole(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2
        )
        s.add(Implies(And(f.parent_isole, f.nb_personnes_a_charge == 1), qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(And(f.parent_isole, f.nb_personnes_a_charge == 2), qf == qf_target))
        qf_target += QuotientFamilial2020(
            2 * enfants_minus_2,
            QuotientFamilial2020.plafond_enfant_a_charge(s) * enfants_minus_2,
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2 * enfants_minus_2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2 * enfants_minus_2
        )
        s.add(Implies(And(f.parent_isole, f.nb_personnes_a_charge >= 3), qf == qf_target))

        # Normal
        parent_normal = Not(Or(f.parent_isole, f.veuf))
        qf_target = QuotientFamilial2020(
            2,
            Montant.euros(0, s),
            QuotientFamilial2020.plafond_num_rscr_celibataire(s),
            QuotientFamilial2020.plafond_rscr_celibataire(s)
        )
        s.add(Implies(And(parent_normal, f.nb_personnes_a_charge == 0), qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(And(parent_normal, f.nb_personnes_a_charge == 1), qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(And(parent_normal, f.nb_personnes_a_charge == 2), qf == qf_target))
        qf_target += QuotientFamilial2020(
            2 * enfants_minus_2,
            QuotientFamilial2020.plafond_enfant_a_charge(s) * enfants_minus_2,
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2 * enfants_minus_2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2 * enfants_minus_2
        )
        s.add(Implies(And(parent_normal, f.nb_personnes_a_charge >= 3), qf == qf_target))

        return qf

    @staticmethod
    def concubinage(f: FoyerFiscal, s: Solver) -> 'QuotientFamilial2020':
        qf = QuotientFamilial2020.anonyme(s)

        enfants_minus_4 = Montant.bitvec_sat_sub(f.nb_personnes_a_charge, 4, s)

        qf_target = QuotientFamilial2020(
            2,
            Montant.euros(0, s),
            QuotientFamilial2020.plafond_num_rscr_celibataire(s),
            QuotientFamilial2020.plafond_rscr_celibataire(s)
        )
        s.add(Implies(f.nb_personnes_a_charge == 0, qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(f.nb_personnes_a_charge == 1, qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(f.nb_personnes_a_charge == 2, qf == qf_target))
        qf_target += QuotientFamilial2020(
            2,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2
        )
        s.add(Implies(f.nb_personnes_a_charge == 3, qf == qf_target))
        qf_target += QuotientFamilial2020(
            2,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2
        )
        s.add(Implies(f.nb_personnes_a_charge == 4, qf == qf_target))

        qf_target = QuotientFamilial2020(
            2 * enfants_minus_4,
            QuotientFamilial2020.plafond_enfant_a_charge(s) * enfants_minus_4,
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2 * enfants_minus_4,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2 * enfants_minus_4
        )
        s.add(Implies(f.nb_personnes_a_charge >= 5, qf == qf_target))

        return qf

    @staticmethod
    def couple(f: FoyerFiscal, s: Solver) -> 'QuotientFamilial2020':
        qf = QuotientFamilial2020.anonyme(s)

        enfants_minus_4 = Montant.bitvec_sat_sub(f.nb_personnes_a_charge, 4, s)

        qf_target = QuotientFamilial2020(
            4,
            Montant.euros(0, s),
            QuotientFamilial2020.plafond_num_rscr_couple(s),
            QuotientFamilial2020.plafond_rscr_couple(s)
        )
        s.add(Implies(f.nb_personnes_a_charge == 0, qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(f.nb_personnes_a_charge == 1, qf == qf_target))
        qf_target += QuotientFamilial2020(
            1,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s),
            QuotientFamilial2020.plafond_rscr_demi_part(s)
        )
        s.add(Implies(f.nb_personnes_a_charge == 2, qf == qf_target))
        qf_target += QuotientFamilial2020(
            2,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2
        )
        s.add(Implies(f.nb_personnes_a_charge == 3, qf == qf_target))
        qf_target += QuotientFamilial2020(
            2,
            QuotientFamilial2020.plafond_enfant_a_charge(s),
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2
        )
        s.add(Implies(f.nb_personnes_a_charge == 4, qf == qf_target))
        qf_target += QuotientFamilial2020(
            2 * enfants_minus_4,
            QuotientFamilial2020.plafond_enfant_a_charge(s) * enfants_minus_4,
            QuotientFamilial2020.plafond_num_rscr_demi_part(s) * 2 * enfants_minus_4,
            QuotientFamilial2020.plafond_rscr_demi_part(s) * 2 * enfants_minus_4
        )
        s.add(Implies(f.nb_personnes_a_charge >= 5, qf == qf_target))

        return qf

    @staticmethod
    def nombre(f: FoyerFiscal, s: Solver) -> 'QuotientFamilial2020':
        base = QuotientFamilial2020.anonyme(s)

        s.add(Implies(
            f.composition == Composition.celibataire(),
            base == QuotientFamilial2020.celibataire(f, s)
        ))
        s.add(Implies(
            f.composition == Composition.concubinage(),
            base == QuotientFamilial2020.concubinage(f, s)
        ))
        s.add(Implies(
            f.composition == Composition.couple(),
            base == QuotientFamilial2020.couple(f, s)
        ))
        invalides_a_charge = f.nb_invalides_a_charge(s)
        return base + QuotientFamilial2020(
            invalides_a_charge,
            QuotientFamilial2020.plafond_enfant_a_charge_invalide(s) * invalides_a_charge,
            Montant.euros(0, s),
            Montant.euros(0, s)
        )


class IR2020:
    counter: int = 0

    def __init__(self, f: FoyerFiscal):
        self.foyer = f
        self.plafonnement_q_f_actif = Bool("plafonnement_q_f_actif{}".format(IR2020.counter))
        IR2020.counter += 1
        self.decote_actif = Bool("decote_actif{}".format(IR2020.counter))
        IR2020.counter += 1
        self.non_prelemevement_actif = Bool("non_prelemevement_actif{}".format(IR2020.counter))
        IR2020.counter += 1

        self.impot_sans_q_f_: Optional[Montant] = None
        self.impot_avec_q_f_: Optional[Montant] = None
        self.montant_: Optional[Montant] = None
        self.calcul_avec_etapes_: Optional[List[Montant]] = None

    @staticmethod
    def denominateur_rscr_celibataire(s: Solver) -> Montant:
        return Montant.euros(2052, s)

    @staticmethod
    def denominateur_rscr_couple(s: Solver) -> Montant:
        return Montant.euros(4104, s)

    @staticmethod
    def montant_selon_revenu(r: Montant, s: Solver) -> Montant:
        return (TrancheIR2020.tranche1(s).montant(r, s) +
        TrancheIR2020.tranche2(s).montant(r, s) +
        TrancheIR2020.tranche3(s).montant(r, s) +
        TrancheIR2020.tranche4(s).montant(r, s) +
        TrancheIR2020.tranche5(s).montant(r, s))

    @staticmethod
    def plafond_decote_celibataire(s: Solver) -> Montant:
        return Montant.euros(777, s)

    @staticmethod
    def plafond_decote_couple(s: Solver) -> Montant:
        return Montant.euros(1286, s)

    def decote(self, ir: Montant, s: Solver) -> Montant:
        plafond = Montant.anonyme(s, "plafond_decote")

        s.add(Implies(
            Or(self.foyer.composition == Composition.celibataire(),
            self.foyer.composition == Composition.concubinage()),
            plafond == IR2020.plafond_decote_celibataire(s)
        ))
        s.add(Implies(
            self.foyer.composition == Composition.couple(),
            plafond == IR2020.plafond_decote_couple(s)
        ))

        decote_montant = plafond.sub(ir % Taux.pourmille(453))

        decote = Montant.anonyme(s, "decote")
        s.add(Implies(decote_montant < Montant.euros(0, s), decote == Montant.euros(0, s)))
        s.add(Implies(decote_montant >= Montant.euros(0, s),
            decote == decote_montant
        ))

        return decote.round_to_euro()

    def impot_sans_q_f(self, s: Solver) -> Montant:
        if self.impot_sans_q_f_ is None:
            rfr = self.foyer.revenu_fiscal_reference(s)
            sans_q_f = QuotientFamilial2020(
                2 * self.foyer.nb_personnes(s),
                Montant.euros(0, s),
                Montant.euros(0, s),
                Montant.euros(0, s)
            )
            rev_sans_q_f = QuotientFamilial2020.diviser(rfr, sans_q_f, s)
            impot_par_part_sans_q_f = IR2020.montant_selon_revenu(rev_sans_q_f, s)
            self.impot_sans_q_f_ = QuotientFamilial2020.multiplier(impot_par_part_sans_q_f, sans_q_f).round_to_euro()
        return self.impot_sans_q_f_

    def impot_avec_q_f(self, s: Solver) -> Montant:
        if self.impot_avec_q_f_ is None:
            rfr = self.foyer.revenu_fiscal_reference(s)
            q_f = QuotientFamilial2020.nombre(self.foyer, s)

            rev_q_f = QuotientFamilial2020.diviser(rfr, q_f, s)
            impot_par_part_q_f = IR2020.montant_selon_revenu(rev_q_f, s)

            self.impot_avec_q_f_ = QuotientFamilial2020.multiplier(impot_par_part_q_f, q_f).round_to_euro()
        return self.impot_avec_q_f_

    def montant(self, s: Solver) -> Montant:
        if self.montant_ is None:
            self.montant_ = (self.calcul_avec_etapes(s))[3]
        return self.montant_

    def calcul_avec_etapes(self, s: Solver) -> List[Montant]:
        if self.calcul_avec_etapes_ is None:

            impot_avec_q_f = self.impot_avec_q_f(s)

            impot_sans_q_f = self.impot_sans_q_f(s)

            q_f = QuotientFamilial2020.nombre(self.foyer, s)

            out1 = Montant.anonyme(s, "ir_out1")
            s.add(Implies(
                impot_sans_q_f.sub(impot_avec_q_f) >= q_f.plafond_q_f,
                And(out1 == impot_sans_q_f.sub(q_f.plafond_q_f),
                self.plafonnement_q_f_actif)
            ))
            s.add(Implies(
                Not(impot_sans_q_f.sub(impot_avec_q_f) >= q_f.plafond_q_f),
                And(out1 == impot_avec_q_f, Not(self.plafonnement_q_f_actif))
            ))

            # Décote
            decote = self.decote(out1, s)

            decote_out = Montant.anonyme(s, "decote_out")
            s.add(Implies(decote <= out1, decote_out == decote))
            s.add(Implies(decote > out1, decote_out == out1))

            out2 = out1.sub(decote)

            out3 = out2

            out = Montant.anonyme(s, "ir_out")
            ir_not_paid = out3 < Montant.euros(60, s)
            s.add(Implies(ir_not_paid,
                And(out == Montant.euros(0, s), self.non_prelemevement_actif)
            ))
            s.add(Implies(Not(ir_not_paid),
                And(out == out3, Not(self.non_prelemevement_actif))
            ))

            rfr = self.foyer.revenu_fiscal_reference(s)

            # Returns : rfr, droits_simples, decote, montant_final
            self.calcul_avec_etapes_ = [rfr, out1, decote_out, out]
        return self.calcul_avec_etapes_


class IRMenage2020:
    def __init__(self, m: Menage):
        self.menage = m
        self.calcul_avec_etapes_: Optional[List[Montant]] = None
        self.montant_: Optional[Montant] = None

    def calcul_avec_etapes(self, s: Solver) -> List[Montant]:
        if self.calcul_avec_etapes_ is None:
            rfr = Montant.anonyme(s, "rfr")
            ds = Montant.anonyme(s, "droits_simples")
            decote = Montant.anonyme(s, "decote")
            out = Montant.anonyme(s, "ir")
            bir1 = IR2020(self.menage.f1)
            bir2 = IR2020(self.menage.f2)
            [rfr1, ds1, decote1, out1] = bir1.calcul_avec_etapes(s)
            [rfr2, ds2, decote2, out2] = bir2.calcul_avec_etapes(s)
            s.add(Implies(
                self.menage.composition == Composition.concubinage(),
                And(rfr == rfr1 + rfr2,
                ds == ds1 + ds2,
                decote == decote1 + decote2,
                out == out1 + out2)
            ))
            s.add(Implies(
                Not(self.menage.composition == Composition.concubinage()),
                And(rfr == rfr1,
                ds == ds1,
                decote == decote1,
                out == out1)
            ))
            self.calcul_avec_etapes_ = [rfr, ds, decote, out]
        return self.calcul_avec_etapes_

    def montant_2020(self, s: Solver) -> Montant:
        if self.montant_ is None:
            self.montant_ = (self.calcul_avec_etapes(s))[3]
        return self.montant_
