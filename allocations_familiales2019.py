# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from z3 import *
from montant import *
from individu import *
from foyerfiscal import *
from solveur import *
from menage import *
from typing import Optional, List, Any


class AllocationsFamiliales2019:

    def __init__(self, m: Menage):
        self.menage = m

        self.complement_familial_: Optional[Montant] = None
        self.paje_: Optional[Montant] = None
        self.allocation_rentree_scolaire_: Optional[Montant] = None
        self.bourses_college_trimestrielles_: Optional[Montant] = None
        self.bourses_lycee_annuelles_: Optional[Montant] = None
        self.calcul_avec_etapes_: Optional[List[Montant]] = None

    counter: int = 0

    def complement_familial(self, s: Solver) -> Montant:
        if self.complement_familial_ is None:
            out = Montant.anonyme(s, "complement_familial")
            nb_enfants_concernes = self.menage.nb_enfants_plus_3_ans_moins_20_ans(s)
            s.add(Implies(nb_enfants_concernes <= 2, out == Montant.euros(0, s)))

            enfants_minus_3 = Montant.bitvec_sat_sub(self.menage.nb_personnes_a_charge(s), 3, s)

            base1 = AllocationsFamiliales2019.base_mensuelle_allocations_familiales(s) % Taux.pourmille(417)
            base2 = AllocationsFamiliales2019.base_mensuelle_allocations_familiales(s) % Taux.pourmille(208)

            plafond1a = Montant.euros(23341, s) + Montant.euros(3180, s) * enfants_minus_3
            plafond2a = Montant.euros(46680, s) + Montant.euros(6360, s) * enfants_minus_3

            plafond1b = Montant.euros(19081, s) + Montant.euros(3180, s) * enfants_minus_3
            plafond2b = Montant.euros(38159, s) + Montant.euros(6360, s) * enfants_minus_3

            deux_revenus_ou_isole = Or(self.menage.f1.parent_isole, And(
                self.menage.composition == Composition.couple(),
                self.menage.f1.individus[0].en_activite,
                self.menage.f1.individus[1].en_activite
            ))
            rfr = self.menage.revenu_fiscal_reference(s)

            s.add(Implies(And(nb_enfants_concernes >= 3, deux_revenus_ou_isole, rfr >= plafond2a),
                out == Montant.euros(0, s)
            ))
            s.add(Implies(And(nb_enfants_concernes >= 3, deux_revenus_ou_isole, rfr >= plafond1a, rfr < plafond2a),
                out == base1
            ))
            s.add(Implies(And(nb_enfants_concernes >= 3, deux_revenus_ou_isole, rfr < plafond1a),
                out == base1 + base2
            ))

            s.add(Implies(And(nb_enfants_concernes >= 3, Not(deux_revenus_ou_isole), rfr >= plafond2b),
                out == Montant.euros(0, s)
            ))
            s.add(Implies(And(nb_enfants_concernes >= 3, Not(deux_revenus_ou_isole), rfr >= plafond1b, rfr < plafond2b),
                out == base1
            ))
            s.add(Implies(And(nb_enfants_concernes >= 3, Not(deux_revenus_ou_isole), rfr < plafond1b),
                out == base1 + base2
            ))

            self.complement_familial_ = out.round_to_euro()
        return self.complement_familial_

    def paje(self, s: Solver) -> Montant:
        if self.paje_ is None:
            out = Montant.anonyme(s, "paje")
            nb_enfants_concernes = self.menage.nb_enfants_moins_3_ans_strict(s)
            s.add(Implies(nb_enfants_concernes <= 0, out == Montant.euros(0, s)))

            enfants_minus_1 = Montant.bitvec_sat_sub(self.menage.nb_personnes_a_charge(s), 1, s)

            base = AllocationsFamiliales2019.base_mensuelle_allocations_familiales(s) % Taux.pourmille(208)

            plafond1a = Montant.euros(38606, s) + Montant.euros(5480, s) * enfants_minus_1
            plafond2a = Montant.euros(46123, s) + Montant.euros(6547, s) * enfants_minus_1

            plafond1b = Montant.euros(30388, s) + Montant.euros(5480, s) * enfants_minus_1
            plafond2b = Montant.euros(36034, s) + Montant.euros(6547, s) * enfants_minus_1

            deux_revenus_ou_isole = Or(self.menage.f1.parent_isole, And(
                self.menage.composition == Composition.couple(),
                self.menage.f1.individus[0].en_activite,
                self.menage.f1.individus[1].en_activite
            ))
            rfr = self.menage.revenu_fiscal_reference(s)

            s.add(Implies(And(nb_enfants_concernes >= 1, deux_revenus_ou_isole, rfr >= plafond2a),
                out == Montant.euros(0, s)
            ))
            s.add(Implies(And(nb_enfants_concernes >= 1, deux_revenus_ou_isole, rfr >= plafond1a, rfr < plafond2a),
                out == base
            ))
            s.add(Implies(And(nb_enfants_concernes >= 1, deux_revenus_ou_isole, rfr < plafond1a),
                out == base * 2
            ))

            s.add(Implies(And(nb_enfants_concernes >= 1, Not(deux_revenus_ou_isole), rfr >= plafond2b),
                out == Montant.euros(0, s)
            ))
            s.add(Implies(And(nb_enfants_concernes >= 1, Not(deux_revenus_ou_isole), rfr >= plafond1b, rfr < plafond2b),
                out == base
            ))
            s.add(Implies(And(nb_enfants_concernes >= 1, Not(deux_revenus_ou_isole), rfr < plafond1b),
                out == base * 2
            ))

            self.paje_ = out.round_to_euro()
        return self.paje_

    def allocation_rentree_scolaire(self, s: Solver) -> Montant:
        if self.allocation_rentree_scolaire_ is None:
            montant = Montant.euros(0, s)
            for (i, indiv) in enumerate(self.menage.f1.personnes_a_charge):
                outi = Montant.anonyme(s, "rentree_scolaire")
                s.add(If(
                    And(i + 1 <= self.menage.f1.nb_personnes_a_charge, indiv.scolarise),
                    If(
                        And(indiv.age >= 6, indiv.age <= 10),
                        outi == Montant.euros(360, s),
                        If(
                            And(indiv.age >= 11, indiv.age <= 14),
                            outi == Montant.euros(390, s),
                            If(
                                And(indiv.age >= 15, indiv.age <= 18),
                                outi == Montant.euros(403, s),
                                outi == Montant.euros(0, s)
                            )
                        )
                    ),
                    outi == Montant.euros(0, s)
                ))
                montant += outi

            for (i, indiv) in enumerate(self.menage.f2.personnes_a_charge):
                outi = Montant.anonyme(s, "rentree_scolaire")
                s.add(If(
                    And(self.menage.composition == Composition.concubinage(),
                    i + 1 <= self.menage.f2.nb_personnes_a_charge, indiv.scolarise),
                    If(
                        And(indiv.age >= 6, indiv.age <= 10),
                        outi == Montant.euros(360, s),
                        If(
                            And(indiv.age >= 11, indiv.age <= 14),
                            outi == Montant.euros(390, s),
                            If(
                                And(indiv.age >= 15, indiv.age <= 18),
                                outi == Montant.euros(403, s),
                                outi == Montant.euros(0, s)
                            )
                        )
                    ),
                    outi == Montant.euros(0, s)
                ))
                montant += outi

            out = Montant.anonyme(s, "rentree_scolaire")
            nb_enfants_concernes = self.menage.nb_personnes_a_charge(s)
            enfants_minus_1 = Montant.bitvec_sat_sub(nb_enfants_concernes, 1, s)
            plafond = Montant.euros(24697, s) + Montant.euros(5699, s) * enfants_minus_1
            rfr = self.menage.revenu_fiscal_reference(s)
            s.add(Implies(rfr > plafond, out == Montant.euros(0, s)))
            s.add(Implies(rfr <= plafond, out == montant))
            self.allocation_rentree_scolaire_ = out
        return self.allocation_rentree_scolaire_

    @staticmethod
    def base_mensuelle_allocations_familiales(s: Solver) -> Montant:
        return Montant.centimes(41316, s)

    def bourses_college_trimestrielles(self, s: Solver) -> Montant:
        if self.bourses_college_trimestrielles_ is None:
            out = Montant.anonyme(s, "bourse_college")

            rfr = self.menage.revenu_salaires(s)
            enfants_minus_1 = Montant.bitvec_sat_sub(self.menage.nb_personnes_a_charge(s), 1, s)

            plafond1 = Montant.euros(2870, s) + Montant.euros(662, s) * enfants_minus_1
            plafond2 = Montant.euros(8134, s) + Montant.euros(1878, s) * enfants_minus_1
            plafond3 = Montant.euros(15048, s) + Montant.euros(3473, s) * enfants_minus_1

            s.add(Implies(rfr >= plafond3,
                out == Montant.euros(0, s)
            ))
            s.add(Implies(And(rfr >= plafond2, rfr < plafond3),
                out == Montant.euros(35, s)
            ))
            s.add(Implies(And(rfr >= plafond1, rfr < plafond2),
                out == Montant.euros(97, s)
            ))
            s.add(Implies(rfr < plafond1,
                out == Montant.euros(152, s)
            ))

            self.bourses_college_trimestrielles_ = out * self.menage.nb_enfants_au_college(s)
        return self.bourses_college_trimestrielles_

    def bourses_lycee_annuelles(self, s: Solver) -> Montant:
        if self.bourses_lycee_annuelles_ is None:
            out = Montant.anonyme(s, "bourse_lycee")

            rfr = self.menage.revenu_fiscal_reference(s)

            echelon_6_1_enfants = Montant.euros(2384, s)
            echelon_5_1_enfants = Montant.euros(6101, s)
            echelon_4_1_enfants = Montant.euros(9817, s)
            echelon_3_1_enfants = Montant.euros(12172, s)
            echelon_2_1_enfants = Montant.euros(14332, s)
            echelon_1_1_enfants = Montant.euros(18105, s)

            echelon_6_2_enfants = Montant.euros(2849, s)
            echelon_5_2_enfants = Montant.euros(6779, s)
            echelon_4_2_enfants = Montant.euros(10708, s)
            echelon_3_2_enfants = Montant.euros(13278, s)
            echelon_2_2_enfants = Montant.euros(15636, s)
            echelon_1_2_enfants = Montant.euros(19497, s)

            echelon_6_3_enfants = Montant.euros(3776, s)
            echelon_5_3_enfants = Montant.euros(8135, s)
            echelon_4_3_enfants = Montant.euros(12494, s)
            echelon_3_3_enfants = Montant.euros(15491, s)
            echelon_2_3_enfants = Montant.euros(18241, s)
            echelon_1_3_enfants = Montant.euros(22281, s)

            echelon_6_4_enfants = Montant.euros(4701, s)
            echelon_5_4_enfants = Montant.euros(9490, s)
            echelon_4_4_enfants = Montant.euros(14279, s)
            echelon_3_4_enfants = Montant.euros(17705, s)
            echelon_2_4_enfants = Montant.euros(20849, s)
            echelon_1_4_enfants = Montant.euros(25763, s)

            echelon_6_5_enfants = Montant.euros(6091, s)
            echelon_5_5_enfants = Montant.euros(11524, s)
            echelon_4_5_enfants = Montant.euros(16956, s)
            echelon_3_5_enfants = Montant.euros(21024, s)
            echelon_2_5_enfants = Montant.euros(24758, s)
            echelon_1_5_enfants = Montant.euros(29245, s)

            echelon_6_6_enfants = Montant.euros(7480, s)
            echelon_5_6_enfants = Montant.euros(13559, s)
            echelon_4_6_enfants = Montant.euros(19635, s)
            echelon_3_6_enfants = Montant.euros(24344, s)
            echelon_2_6_enfants = Montant.euros(28666, s)
            echelon_1_6_enfants = Montant.euros(33424, s)

            montant_echelon1 = Montant.euros(438, s)
            montant_echelon2 = Montant.euros(540, s)
            montant_echelon3 = Montant.euros(636, s)
            montant_echelon4 = Montant.euros(732, s)
            montant_echelon5 = Montant.euros(831, s)
            montant_echelon6 = Montant.euros(930, s)

            def add_line(s, nb_enfants, plafond6, plafond5, plafond4, plafond3, plafond2, plafond1):  # type: ignore
                s.add(Implies(
                    And(self.menage.nb_personnes_a_charge(s) == nb_enfants, rfr < plafond6),
                    out == montant_echelon6
                ))
                s.add(Implies(
                    And(self.menage.nb_personnes_a_charge(s) == nb_enfants, rfr < plafond5, rfr >= plafond6),
                    out == montant_echelon5
                ))
                s.add(Implies(
                    And(self.menage.nb_personnes_a_charge(s) == nb_enfants, rfr < plafond4, rfr >= plafond5),
                    out == montant_echelon4
                ))
                s.add(Implies(
                    And(self.menage.nb_personnes_a_charge(s) == nb_enfants, rfr < plafond3, rfr >= plafond4),
                    out == montant_echelon3
                ))
                s.add(Implies(
                    And(self.menage.nb_personnes_a_charge(s) == nb_enfants, rfr < plafond2, rfr >= plafond3),
                    out == montant_echelon2
                ))
                s.add(Implies(
                    And(self.menage.nb_personnes_a_charge(s) == nb_enfants, rfr < plafond1, rfr >= plafond2),
                    out == montant_echelon1
                ))
                s.add(Implies(
                    And(self.menage.nb_personnes_a_charge(s) == nb_enfants, rfr >= plafond1),
                    out == Montant.euros(0, s)
                ))

            s.add(Implies(self.menage.nb_personnes_a_charge(s) == 0, out == Montant.euros(0, s)))
            add_line(s, 1, echelon_6_1_enfants, echelon_5_1_enfants, echelon_4_1_enfants,
                echelon_3_1_enfants, echelon_2_1_enfants, echelon_1_1_enfants)
            add_line(s, 2, echelon_6_2_enfants, echelon_5_2_enfants, echelon_4_2_enfants,
                echelon_3_2_enfants, echelon_2_2_enfants, echelon_1_2_enfants)
            add_line(s, 3, echelon_6_3_enfants, echelon_5_3_enfants, echelon_4_3_enfants,
                echelon_3_3_enfants, echelon_2_3_enfants, echelon_1_3_enfants)
            add_line(s, 4, echelon_6_4_enfants, echelon_5_4_enfants, echelon_4_4_enfants,
                echelon_3_4_enfants, echelon_2_4_enfants, echelon_1_4_enfants)
            add_line(s, 5, echelon_6_5_enfants, echelon_5_5_enfants, echelon_4_5_enfants,
                echelon_3_5_enfants, echelon_2_5_enfants, echelon_1_5_enfants)
            add_line(s, 6, echelon_6_6_enfants, echelon_5_6_enfants, echelon_4_6_enfants,
                echelon_3_6_enfants, echelon_2_6_enfants, echelon_1_6_enfants)

            self.bourses_lycee_annuelles_ = out * self.menage.nb_enfants_au_lycee(s)
        return self.bourses_lycee_annuelles_

    def calcul_avec_etapes(self, s: Solver) -> List[Montant]:
        if self.calcul_avec_etapes_ is None:
            out = Montant.anonyme(s, "allocations_familiales")
            nb_enfants_moins_20_ans_strict = self.menage.nb_enfants_moins_20_ans_strict(s)

            enfants_minus_2 = Montant.bitvec_sat_sub(nb_enfants_moins_20_ans_strict, 2, s)
            indic = BitVec("indic{}".format(AllocationsFamiliales2019.counter), repr_bits)
            AllocationsFamiliales2019.counter += 1
            s.add(Implies(nb_enfants_moins_20_ans_strict <= 1, indic == 0))
            s.add(Implies(nb_enfants_moins_20_ans_strict >= 2, indic == 1))

            base = ((AllocationsFamiliales2019.base_mensuelle_allocations_familiales(s) % Taux.pourcent(32)) * indic +
              (AllocationsFamiliales2019.base_mensuelle_allocations_familiales(s) % Taux.pourcent(41)) * enfants_minus_2).round_to_euro()

            nb_enfants_plus_14_ans = self.menage.nb_enfants_plus_14_ans_moins_20_ans_strict(s)
            mult = If(nb_enfants_moins_20_ans_strict > 2, nb_enfants_plus_14_ans, BitVecVal(0, repr_bits))
            majoration14 = ((AllocationsFamiliales2019.base_mensuelle_allocations_familiales(s) % Taux.pourcent(16)) * mult).round_to_euro()

            nb_enfants_20_ans = self.menage.nb_enfants_20_ans(s)
            mult = If(nb_enfants_moins_20_ans_strict + nb_enfants_20_ans >= 3, nb_enfants_20_ans, BitVecVal(0, repr_bits))
            majoration20 = ((AllocationsFamiliales2019.base_mensuelle_allocations_familiales(s) % Taux.pourmille(202)) * mult).round_to_euro()

            complement_familial = self.complement_familial(s)
            paje = self.paje(s)
            complement = Montant.anonyme(s, "complement_allocations_familiales")
            s.add(Implies(complement_familial > paje, complement == complement_familial))
            s.add(Implies(complement_familial <= paje, complement == paje))

            montant = (base +
                majoration14 +
                majoration20)
            rfr = self.menage.revenu_fiscal_reference(s)

            plafond1 = Montant.euros(68217, s) + Montant.euros(5864, s) * enfants_minus_2
            plafond2 = Montant.euros(90926, s) + Montant.euros(5864, s) * enfants_minus_2

            montant_sur_deux = montant // 2
            montant_sur_quatre = montant // 4

            nb_enfants_moins_20_ans = nb_enfants_moins_20_ans_strict + nb_enfants_20_ans

            s.add(Implies(
                And(nb_enfants_moins_20_ans >= 2, rfr >= plafond2),
                out == montant_sur_quatre + montant_sur_quatre.sub(rfr - plafond2)
            ))
            s.add(Implies(
                And(nb_enfants_moins_20_ans >= 2, rfr >= plafond1, rfr < plafond2),
                out == montant_sur_deux + montant_sur_deux.sub(rfr - plafond1)
            ))
            s.add(Implies(
                And(nb_enfants_moins_20_ans >= 2, rfr < plafond1),
                out == montant
            ))
            s.add(Implies(
                nb_enfants_moins_20_ans < 2,
                out == Montant.euros(0, s)
            ))
            out = out + complement

            self.calcul_avec_etapes_ = [base, majoration14 + majoration20, complement_familial, paje, out.round_to_euro()]
        return self.calcul_avec_etapes_
