# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from montant import *
from typing import Any, List, Union, Optional
from tabulate import tabulate
from individu import *


class Composition:
    def __init__(self, code: Union[int, BitVec, BitVecVal]):
        self.code = code

    def __eq__(self, rhs: Any) -> Any:
        return self.code == rhs.code

    def __ne__(self, rhs: Any) -> Any:
        return self.code != rhs.code

    @staticmethod
    def celibataire() -> 'Composition':
        return Composition(BitVecVal(0, repr_bits))

    @staticmethod
    def concubinage() -> 'Composition':
        return Composition(BitVecVal(1, repr_bits))

    @staticmethod
    def couple() -> 'Composition':
        return Composition(BitVecVal(2, repr_bits))

    @staticmethod
    def variable(name: str, s: Solver) -> 'Composition':
        var = BitVec(name + "_composition_", repr_bits)
        s.add(Or(var == 0, var == 1, var == 2))
        return Composition(var)

    def evaluate(self, s: Solver) -> 'Composition':
        return Composition(s.model().evaluate(self.code).as_long())

    def __str__(self) -> str:
        if self.code == 0:
            return "célibataire"
        elif self.code == 1:
            return "concubinage"
        else:
            return "couple"


class FoyerFiscal:
    max_a_charge: int = 6

    counter: int = 0

    def __init__(
        self,
        composition: Composition,
        parent_isole: Union[bool, Bool],
        veuf: Union[bool, Bool],
        individus: List[Individu],
        personnes_a_charge: List[Individu],
        nb_personnes_a_charge: Union[int, BitVec]
    ):
        self.composition = composition
        self.nb_personnes_a_charge = nb_personnes_a_charge
        self.personnes_a_charge = personnes_a_charge
        self.parent_isole = parent_isole
        self.veuf = veuf
        if len(individus) != 2:
            raise TypeError("there should be 2 individus no matter what")
        if len(personnes_a_charge) != FoyerFiscal.max_a_charge:
            raise TypeError("there should be {} personnes_a_charge no matter what".format(FoyerFiscal.max_a_charge))
        self.individus = individus

        self.revenus_salaires_: Optional[Montant] = None
        self.r_fiscal_reference_: Optional[Montant] = None
        self.nb_personnes_: Optional[BitVec] = None
        self.nb_invalides_a_charge_: Optional[BitVecVal] = None
        self.nb_enfants_plus_14_ans_moins_20_ans_strict_: Optional[BitVecVal] = None
        self.nb_enfants_moins_3_ans_strict_: Optional[BitVecVal] = None
        self.nb_enfants_plus_3_ans_moins_20_ans_: Optional[BitVecVal] = None
        self.nb_enfants_moins_20_ans_strict_: Optional[BitVecVal] = None
        self.nb_enfants_moins_20_ans_: Optional[BitVecVal] = None
        self.nb_enfants_20_ans_: Optional[BitVecVal] = None
        self.nb_enfants_au_college_: Optional[BitVecVal] = None
        self.nb_enfants_au_lycee_: Optional[BitVecVal] = None
        self.salaires_3_derniers_mois_indiv1_: Optional[Montant] = None

    def revenu_salaires(self, s: Solver) -> Montant:
        if self.revenus_salaires_ is None:
            rev0 = self.individus[0].revenu_annuel(s)
            rev1 = self.individus[1].revenu_annuel(s)
            out = Montant.anonyme(s, "revenu_salaires")
            s.add(Implies(self.une_personne(), rev0 == out))
            s.add(Implies(self.deux_personnes(), rev1 + rev0 == out))
            self.revenus_salaires_ = out
        return self.revenus_salaires_

    def revenu_fiscal_reference(self, s: Solver) -> Montant:
        # HYPOTHÈSE: le revenu fiscal de référenceest uniquement alimenté par les salaires
        if self.r_fiscal_reference_ is None:
            rfr = Montant.anonyme(s, "rfr")
            s.add(rfr == (self.revenu_salaires(s) % Taux.pourcent(90)).round_to_euro())
            self.r_fiscal_reference_ = rfr
        return self.r_fiscal_reference_

    def salaires_3_derniers_mois_indiv0(self) -> Montant:
        return self.individus[0].salaires_3_derniers_mois()

    def salaires_3_derniers_mois_indiv1(self, s: Solver) -> Montant:
        if self.salaires_3_derniers_mois_indiv1_ is None:
            out = Montant.anonyme(s, "salaires_3_derniers_mois_indiv1")
            s.add(Implies(
                Or(self.composition == Composition.celibataire(),
                self.composition == Composition.concubinage()),
                out == Montant.euros(0, s)
            ))
            s.add(Implies(
                self.composition == Composition.couple(),
                out == self.individus[1].salaires_3_derniers_mois()
            ))
            self.salaires_3_derniers_mois_indiv1_ = out
        return self.salaires_3_derniers_mois_indiv1_

    def ok(self, s: Solver) -> Any:
        # Les personnes à charge ont toutes un id disjoint de telle sorte
        # à ce que la concaténation de deux listes de personnes à charge puisse
        # se faire correctement
        flat_list = []
        non_flat_list = [
            [
                self.personnes_a_charge[i].id != self.personnes_a_charge[j].id
                if (i != j) else
                True
                for j in range(FoyerFiscal.max_a_charge)
            ]
            for i in range(FoyerFiscal.max_a_charge)
        ]
        for sublist in non_flat_list:
            for item in sublist:
                flat_list.append(item)
        return And(
            # HYPOTHÈSE: maximum 6 personnes à charge par foyer fiscal
            self.nb_personnes_a_charge <= FoyerFiscal.max_a_charge,
            self.nb_personnes_a_charge >= 0,
            Not(And(self.veuf, self.parent_isole)),
            Implies(Or(self.veuf, self.parent_isole), self.composition ==
                Composition.celibataire()),
            Implies(And(self.composition == Composition.celibataire(),
                self.nb_personnes_a_charge > 0), self.parent_isole),
            Implies(self.parent_isole, self.nb_personnes_a_charge > 0),
            # HYPOTHÈSE: les personnes à charge ne travaillent pas
            Not(self.personnes_a_charge[0].en_activite),
            Not(self.personnes_a_charge[1].en_activite),
            Not(self.personnes_a_charge[2].en_activite),
            Not(self.personnes_a_charge[3].en_activite),
            Not(self.personnes_a_charge[4].en_activite),
            Not(self.personnes_a_charge[5].en_activite),
            # HYPOTHÈSE: les personnes à charge ont moins de 25 ans
            self.personnes_a_charge[0].age <= 25,
            self.personnes_a_charge[1].age <= 25,
            self.personnes_a_charge[2].age <= 25,
            self.personnes_a_charge[3].age <= 25,
            self.personnes_a_charge[4].age <= 25,
            self.personnes_a_charge[5].age <= 25,
            # HYPOTHÈSE: les adultes du foyer ont entre 20 et 60 ans
            self.individus[0].age >= 20, self.individus[0].age <= 60,
            self.individus[1].age >= 20, self.individus[0].age <= 60,
            # HYPOTHÈSE: les adultes du foyer ont 25 ans de plus que le plus vieux de leurs enfants
            self.individus[0].age >= self.personnes_a_charge[0].age + 25,
            self.individus[1].age >= self.personnes_a_charge[0].age + 25,
            self.individus[0].age >= self.personnes_a_charge[1].age + 25,
            self.individus[1].age >= self.personnes_a_charge[1].age + 25,
            self.individus[0].age >= self.personnes_a_charge[2].age + 25,
            self.individus[1].age >= self.personnes_a_charge[2].age + 25,
            self.individus[0].age >= self.personnes_a_charge[3].age + 25,
            self.individus[1].age >= self.personnes_a_charge[3].age + 25,
            self.individus[0].age >= self.personnes_a_charge[4].age + 25,
            self.individus[1].age >= self.personnes_a_charge[4].age + 25,
            self.individus[0].age >= self.personnes_a_charge[5].age + 25,
            self.individus[1].age >= self.personnes_a_charge[5].age + 25,
            And(flat_list)
        )

    @staticmethod
    def variable(name: str, s: Solver) -> 'FoyerFiscal':
        composition = Composition.variable(name + "_composition", s)
        enfants_a_charge = BitVec(name + "_enfants_a_charge", repr_bits)
        invalides_a_charge = BitVec(name + "_invalides_a_charge", repr_bits)
        enfants_a_charge_plus_de_14_ans = BitVec(name + "_enfants_a_charge_plus_de_14_ans", repr_bits)
        parent_isole = Bool(name + "_parent_isole")
        veuf = Bool(name + "_veuf")
        indiv1 = Individu.variable(name + "__individu1", s, is_adulte=True)
        indiv2 = Individu.variable(name + "__individu2", s, is_adulte=True)
        a_charge = [Individu.variable(name + "__a_charge_{}".format(i), s, is_adulte=False) for i in range(FoyerFiscal.max_a_charge)]
        nb_personnes_a_charge = BitVec(name + "__nb_a_charge", repr_bits)
        foyer = FoyerFiscal(
            composition,
            parent_isole,
            veuf,
            [indiv1, indiv2],
            a_charge,
            nb_personnes_a_charge
        )
        s.add(foyer.ok(s))
        return foyer

    def nb_personnes(self, s: Solver) -> BitVec:
        if self.nb_personnes_ is None:
            out = BitVec("nb_personnes{}".format(FoyerFiscal.counter), repr_bits)
            FoyerFiscal.counter += 1
            s.add(Implies(self.une_personne(), out == 1))
            s.add(Implies(self.deux_personnes(), out == 2))
            self.nb_personnes_ = out
        return self.nb_personnes_

    def une_personne(self) -> Any:
        return Or(self.composition == Composition.celibataire(),
        self.composition == Composition.concubinage())

    def deux_personnes(self) -> Any:
        return self.composition == Composition.couple()

    def nb_invalides_a_charge(self, s: Solver) -> BitVecVal:
        if self.nb_invalides_a_charge_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.invalide),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_invalides_a_charge_ = out
        return self.nb_invalides_a_charge_

    def nb_enfants_plus_14_ans_moins_20_ans_strict(self, s: Solver) -> BitVecVal:
        if self.nb_enfants_plus_14_ans_moins_20_ans_strict_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.age >= 14, indiv.age < 20),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_enfants_plus_14_ans_moins_20_ans_strict_ = out
        return self.nb_enfants_plus_14_ans_moins_20_ans_strict_

    def nb_enfants_age_exact(self, s: Solver, age: int) -> BitVecVal:
        out = BitVecVal(0, repr_bits)
        for (i, indiv) in enumerate(self.personnes_a_charge):
            out += If(
                And(i + 1 <= self.nb_personnes_a_charge, indiv.age == age),
                BitVecVal(1, repr_bits),
                BitVecVal(0, repr_bits)
            )
        return out

    def nb_enfants_moins_3_ans_strict(self, s: Solver) -> BitVecVal:
        if self.nb_enfants_moins_3_ans_strict_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.age < 3),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_enfants_moins_3_ans_strict_ = out
        return self.nb_enfants_moins_3_ans_strict_

    def nb_enfants_plus_3_ans_moins_20_ans(self, s: Solver) -> BitVecVal:
        if self.nb_enfants_plus_3_ans_moins_20_ans_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.age >= 3, indiv.age <= 20),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_enfants_plus_3_ans_moins_20_ans_ = out
        return self.nb_enfants_plus_3_ans_moins_20_ans_

    def nb_enfants_moins_20_ans_strict(self, s: Solver) -> BitVecVal:
        if self.nb_enfants_moins_20_ans_strict_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.age < 20),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_enfants_moins_20_ans_strict_ = out
        return self.nb_enfants_moins_20_ans_strict_

    def nb_enfants_20_ans(self, s: Solver) -> BitVecVal:
        if self.nb_enfants_20_ans_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.age == 20),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_enfants_20_ans_ = out
        return self.nb_enfants_20_ans_

    def nb_enfants_au_college(self, s: Solver) -> BitVecVal:
        if self.nb_enfants_au_college_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.age >= 11, indiv.age <= 14, indiv.scolarise),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_enfants_au_college_ = out
        return self.nb_enfants_au_college_

    def nb_enfants_au_lycee(self, s: Solver) -> BitVecVal:
        if self.nb_enfants_au_lycee_ is None:
            out = BitVecVal(0, repr_bits)
            for (i, indiv) in enumerate(self.personnes_a_charge):
                out += If(
                    And(i + 1 <= self.nb_personnes_a_charge, indiv.age >= 15, indiv.age <= 18, indiv.scolarise),
                    BitVecVal(1, repr_bits),
                    BitVecVal(0, repr_bits)
                )
            self.nb_enfants_au_lycee_ = out
        return self.nb_enfants_au_lycee_

    def salaire_constant(self, s: Solver) -> None:
        for i in range(0, len(self.individus)):
            self.individus[i].salaire_constant(s)

    def evaluate(self, s: Solver) -> 'FoyerFiscal':
        return FoyerFiscal(
            self.composition.evaluate(s),
            s.model().evaluate(self.parent_isole),
            s.model().evaluate(self.veuf),
            [self.individus[0].evaluate(s), self.individus[1].evaluate(s)],
            [self.personnes_a_charge[i].evaluate(s) for i in range(FoyerFiscal.max_a_charge)],
            s.model().evaluate(self.nb_personnes_a_charge),
        )

    def augmentation_salaire_premier_individu(self, m: Montant) -> 'FoyerFiscal':
        return FoyerFiscal(
            self.composition,
            self.parent_isole,
            self.veuf,
            [self.individus[0].augmentation_salaire(m), self.individus[1]],
            self.personnes_a_charge,
            self.nb_personnes_a_charge,
        )

    def augmentation_salaire_tous_individus(self, m: Montant, s: Solver) -> 'FoyerFiscal':
        return FoyerFiscal(
            self.composition,
            self.parent_isole,
            self.veuf,
            [self.individus[0].augmentation_salaire(m), self.individus[1].augmentation_salaire(m)],
            self.personnes_a_charge,
            self.nb_personnes_a_charge,
        )

    def nouvel_enfant(self, s: Solver, age: Optional[int]) -> 'FoyerFiscal':
        s.add(self.nb_personnes_a_charge + 1 <= FoyerFiscal.max_a_charge)
        out = FoyerFiscal(
            self.composition,
            self.parent_isole,
            self.veuf,
            self.individus,
            self.personnes_a_charge,
            self.nb_personnes_a_charge + 1,
        )
        if age is not None:
            nb_age_avant = self.nb_enfants_age_exact(s, age)
            nb_age_apres = out.nb_enfants_age_exact(s, age)
            s.add(nb_age_apres == nb_age_avant + 1)
        return out

    def variation_nb_personnes_charge(self, s: Solver, max_variation: int) -> 'FoyerFiscal':
        new_nb_charge = BitVec("nouveau_enfants_a_charge_{}".format(FoyerFiscal.counter), repr_bits)
        s.add(new_nb_charge >= 0)
        s.add(new_nb_charge <= FoyerFiscal.max_a_charge)
        FoyerFiscal.counter += 1
        s.add(self.nb_personnes_a_charge + new_nb_charge <= FoyerFiscal.max_a_charge)
        s.add(self.nb_personnes_a_charge - new_nb_charge >= 0)
        s.add(self.nb_personnes_a_charge - new_nb_charge <= max_variation)
        s.add(new_nb_charge - self.nb_personnes_a_charge <= max_variation)
        return FoyerFiscal(
            self.composition,
            self.parent_isole,
            self.veuf,
            self.individus,
            self.personnes_a_charge,
            self.nb_personnes_a_charge + new_nb_charge,
        )

    def variation_revenus_salaires(self, max_variation_revenus: int, s: Solver) -> 'FoyerFiscal':
        name = "revenu_apres_variation_{}".format(FoyerFiscal.counter)
        FoyerFiscal.counter += 1
        i1 = self.individus[0].same_individu_but_different_revenue(name, s)
        i2 = self.individus[1].same_individu_but_different_revenue(name, s)
        old_rev1 = self.individus[0].revenu_annuel(s)
        old_rev2 = self.individus[1].revenu_annuel(s)
        new_rev1 = i1.revenu_annuel(s)
        new_rev2 = i2.revenu_annuel(s)
        i1_revenue_variation = Montant.anonyme(s, "_variation_i1_{}".format(FoyerFiscal.counter))
        i2_revenue_variation = Montant.anonyme(s, "_variation_i2_{}".format(FoyerFiscal.counter))
        FoyerFiscal.counter += 2
        s.add(Implies(
            new_rev1 - old_rev1 >= Montant.euros(0, s),
            i1_revenue_variation == new_rev1 - old_rev1
        ))
        s.add(Implies(
            new_rev1 - old_rev1 < Montant.euros(0, s),
            i1_revenue_variation == old_rev1 - new_rev1
        ))
        s.add(Implies(
            Or(self.composition == Composition.celibataire(),
            self.composition == Composition.concubinage()),
            i2_revenue_variation == Montant.euros(0, s)
        ))
        s.add(Implies(
            And(new_rev2 - old_rev2 >= Montant.euros(0, s),
            self.composition == Composition.couple()),
            i2_revenue_variation == new_rev2 - old_rev2
        ))
        s.add(Implies(
            And(new_rev2 - old_rev2 < Montant.euros(0, s),
            self.composition == Composition.couple()),
            i2_revenue_variation == old_rev2 - new_rev2
        ))
        s.add(i1_revenue_variation + i2_revenue_variation <= Montant.euros(max_variation_revenus, s))
        return FoyerFiscal(
            self.composition,
            self.parent_isole,
            self.veuf,
            [i1, i2],
            self.personnes_a_charge,
            self.nb_personnes_a_charge,
        )

    def same_foyer_fiscal_different_revenue(self, name: str, s: Solver) -> 'FoyerFiscal':
        i1 = self.individus[0].same_individu_but_different_revenue(name, s)
        i2 = self.individus[1].same_individu_but_different_revenue(name, s)
        return FoyerFiscal(
            self.composition,
            self.parent_isole,
            self.veuf,
            [i1, i2],
            self.personnes_a_charge,
            self.nb_personnes_a_charge,
        )

    def perte_conjoint(self) -> 'FoyerFiscal':
        return FoyerFiscal(
            Composition.celibataire(),
            self.parent_isole,
            True,
            self.individus,
            self.personnes_a_charge,
            self.nb_personnes_a_charge,
        )

    def __str__(self) -> str:
        tablef = str(tabulate(
            [
                ["Situation familiale", self.composition],
                ["Nombre de personnes à charge", self.nb_personnes_a_charge],
                ["Parent isolé", self.parent_isole],
                ["Veuf ou veuve", self.veuf]
            ],
            ["Caractéristique", "Valeur"],
            tablefmt="pipe"
        ))
        if self.composition == Composition.celibataire() or self.composition == Composition.concubinage():
            tableindiv = Individu.list_to_str([self.individus[0]])
        else:
            tableindiv = Individu.list_to_str(self.individus)

        tableACharge = Individu.list_to_str(self.personnes_a_charge[:self.nb_personnes_a_charge.as_long()])  # type: ignore
        return "{}\n\nIndividus du foyer :\n\n{}\n\nPersonnes à charge:\n\n{}".format(tablef, tableindiv, tableACharge)
