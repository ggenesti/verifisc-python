# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from montant import *
from foyerfiscal import *
from z3 import *
from typing import Union, Optional


class Logement:
    def __init__(self,
        code: Union[int, BitVec],
        mensualite: Montant,
        zone: Union[int, BitVec],
    ):
        # HYPOTHÈSE: logement sous forme d'hébergement gratuit ou de location seulement
        self.code = code
        self.mensualite = mensualite
        self.zone = zone

    def __eq__(self, rhs: Any) -> Any:
        return And(self.code == rhs.code,
            self.mensualite == rhs.mensualite,
            self.zone == rhs.zone)

    def __ne__(self, rhs: Any) -> Any:
        return Not(self == rhs)

    @staticmethod
    def hebergement_gratuit(s: Solver, zone: int) -> 'Logement':
        if not (zone in [1, 2, 3]):
            raise TypeError("La zone d'un logement doit être I, II ou III")
        return Logement(0, Montant.euros(0, s), zone)

    @staticmethod
    def location(loyer: Montant, zone: Union[int, BitVec]) -> 'Logement':
        if not (zone in [1, 2, 3]):
            raise TypeError("La zone d'un logement doit être I, II ou III")
        return Logement(1, loyer, zone)

    def is_herbergement_gratuit(self) -> Union[bool, Bool]:
        return self.code == 0

    def is_location(self) -> Union[bool, Bool]:
        return self.code == 1

    def assert_ok(self, s: Solver) -> None:
        # HYPOTHÈSE: loyer en zone 3 toujours supérieur à 300 €
        s.add(Implies(
            And(self.is_location(), self.zone == 3),
            self.mensualite >= Montant.euros(300, s)
        ))
        # HYPOTHÈSE: loyer en zone 2 toujours supérieur à 400 €
        s.add(Implies(
            And(self.is_location(), self.zone == 2),
            self.mensualite >= Montant.euros(400, s)
        ))
        # HYPOTHÈSE: loyer en zone 2 toujours supérieur à 500 €
        s.add(Implies(
            And(self.is_location(), self.zone == 1),
            self.mensualite >= Montant.euros(500, s)
        ))

    @staticmethod
    def variable(name: str, s: Solver) -> 'Logement':
        code = BitVec(name + "_code", repr_bits)
        s.add(Or(code == 0, code == 1))
        zone = BitVec(name + "_zone", repr_bits)
        s.add(Or(zone == 1, zone == 2, zone == 3))
        mensualite = Montant.anonyme(s, name + "_mensualite_logement")
        s.add(Implies(
            code == 0, mensualite == Montant.euros(0, s)
        ))
        s.add(Implies(
            # HYPOTHÈSE: loyer toujours inférieur à 10000 € par mois
            code == 1, mensualite <= Montant.euros(10000, s)
        ))
        logement = Logement(code, mensualite, zone)
        logement.assert_ok(s)
        return logement

    def variation_mensualite(self, max_variation: int, s: Solver) -> 'Logement':
        new_mensualite = Montant.anonyme(s, "_nouvelle_mensualite_logement")
        s.add(And(
            new_mensualite - self.mensualite <= Montant.euros(max_variation, s),
            self.mensualite - new_mensualite <= Montant.euros(max_variation, s)
        ))
        return Logement(
            self.code,
            new_mensualite,
            self.zone
        )

    def evaluate(self, s: Solver) -> 'Logement':
        return Logement(
            s.model().evaluate(self.code),
            self.mensualite.evaluate(s),
            s.model().evaluate(self.zone),
        )

    def __str__(self) -> str:

        if self.code == 0:
            return str(tabulate(
                [
                    ["Type de logement", "Hébergement gratuit"]
                ],
                ["Caractéristique", "Valeur"],
                tablefmt="pipe"
            ))
        elif self.code == 1:
            return str(tabulate(
                [
                    ["Type de logement", "Location"],
                    ["Loyer mensuel", self.mensualite],
                    ["Zone", "I" if self.zone == 1 else ("II" if self.zone == 2 else "III")]
                ],
                ["Caractéristique", "Valeur"],
                tablefmt="pipe"
            ))
        else:
            return str(tabulate(
                [
                    ["Type de logement", "Indéterminé"]
                ],
                ["Caractéristique", "Valeur"],
                tablefmt="pipe"
            ))


class Menage:
    counter: int = 0

    def __init__(self,
        f1: FoyerFiscal,
        f2: FoyerFiscal,
        composition: Composition,
        logement: Logement
    ):
        self.f1 = f1
        self.f2 = f2
        self.composition = composition
        self.logement = logement

        self.nb_invalides_a_charge_: Optional[BitVec] = None
        self.nb_personnes_a_charge_: Optional[BitVec] = None
        self.nombre_personnes_: Optional[BitVec] = None
        self.nb_enfants_plus_3_ans_moins_20_ans_: Optional[BitVec] = None
        self.nb_enfants_moins_3_ans_strict_: Optional[BitVec] = None
        self.nb_enfants_moins_20_ans_strict_: Optional[BitVec] = None
        self.nb_enfants_moins_20_ans_: Optional[BitVec] = None
        self.nb_enfants_plus_14_ans_moins_20_ans_strict_: Optional[BitVec] = None
        self.nb_enfants_20_ans_: Optional[BitVec] = None
        self.nb_enfants_au_college_: Optional[BitVec] = None
        self.nb_enfants_au_lycee_: Optional[BitVec] = None
        self.revenu_salaires_: Optional[Montant] = None
        self.revenu_fiscal_reference_: Optional[Montant] = None
        self.salaires_3_derniers_mois_indiv1_: Optional[Montant] = None
        self.salaires_3_derniers_mois_menage_: Optional[Montant] = None

    def assert_ok(self, s: Solver) -> None:
        s.add(Implies(
            self.composition == Composition.concubinage(),
            And(self.f1.composition == Composition.concubinage(),
            self.f2.composition == Composition.concubinage())
        ))
        s.add(Implies(
            self.composition != Composition.concubinage(),
            self.f1.composition == self.composition
        ))
        # HYPOTHÈSE: loyer/mensualité inférieure au salaire
        s.add(self.logement.mensualite * 12 <= self.revenu_salaires(s))

    def ratio_loyer_salaire(self, min: Taux, max: Taux, s: Solver) -> None:
        s.add(self.logement.mensualite * 12 <= self.revenu_salaires(s) % max)
        s.add(self.logement.mensualite * 12 >= self.revenu_salaires(s) % min)

    def mensualite_minimale(
        self,
        min_base_rent: int,
        min_per_person_rent_increment: int,
        zone_3_percent_multiplier: int,
        zone_2_percent_multiplier: int,
        zone_1_percent_multiplier: int,
        s: Solver
    ) -> None:
        min_rent = (Montant.euros(min_base_rent, s) +
            Montant.euros(min_per_person_rent_increment, s) * self.nombre_personnes(s))
        min_rent_mult = Montant.anonyme(s, "mensualite_minimale")
        s.add(Implies(
            self.logement.zone == 3, min_rent_mult == min_rent % Taux.pourcent(zone_3_percent_multiplier)
        ))
        s.add(Implies(
            self.logement.zone == 2, min_rent_mult == min_rent % Taux.pourcent(zone_2_percent_multiplier)
        ))
        s.add(Implies(
            self.logement.zone == 1, min_rent_mult == min_rent % Taux.pourcent(zone_1_percent_multiplier)
        ))
        s.add(self.logement.mensualite >= min_rent_mult)

    @staticmethod
    def variable(name: str, s: Solver) -> "Menage":
        f1 = FoyerFiscal.variable(name + "_f1", s)
        f2 = FoyerFiscal.variable(name + "_f2", s)
        composition = Composition.variable(name + "_composition", s)
        logement = Logement.variable(name + "_logement", s)
        out = Menage(f1, f2, composition, logement)
        out.assert_ok(s)
        return out

    def evaluate(self, s: Solver) -> 'Menage':
        return Menage(
            self.f1.evaluate(s),
            self.f2.evaluate(s),
            self.composition.evaluate(s),
            self.logement.evaluate(s),
        )

    def nb_invalides_a_charge(self, s: Solver) -> BitVec:
        if self.nb_invalides_a_charge_ is None:
            out = BitVec("nb_invalides_menage{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_invalides_a_charge(s) + self.f2.nb_invalides_a_charge(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_invalides_a_charge(s)
            ))
            self.nb_invalides_a_charge_ = out
        return self.nb_invalides_a_charge_

    def nb_personnes_a_charge(self, s: Solver) -> BitVec:
        if self.nb_personnes_a_charge_ is None:
            out = BitVec("nb_personnes_menage{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_personnes_a_charge + self.f2.nb_personnes_a_charge
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_personnes_a_charge
            ))
            self.nb_personnes_a_charge_ = out
        return self.nb_personnes_a_charge_

    def nombre_personnes(self, s: Solver) -> BitVec:
        if self.nombre_personnes_ is None:
            personnes = BitVec("prime_activite_personnes{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.celibataire(),
                personnes == 1
            ))
            s.add(Implies(
                Or(self.composition == Composition.couple(),
                self.composition == Composition.concubinage()),
                personnes == 2
            ))

            self.nombre_personnes_ = personnes + self.nb_personnes_a_charge(s)
        return self.nombre_personnes_

    def salaires_3_derniers_mois_indiv0(self) -> Montant:
        return self.f1.salaires_3_derniers_mois_indiv0()

    def salaires_3_derniers_mois_indiv1(self, s: Solver) -> Montant:
        if self.salaires_3_derniers_mois_indiv1_ is None:
            out = Montant.anonyme(s, "salaires_3_derniers_mois_indiv1_menage")
            s.add(Implies(
                self.composition == Composition.celibataire(),
                out == Montant.euros(0, s)
            ))
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f2.salaires_3_derniers_mois_indiv0()
            ))
            s.add(Implies(
                self.composition == Composition.couple(),
                out == self.f1.salaires_3_derniers_mois_indiv1(s)
            ))
            self.salaires_3_derniers_mois_indiv1_ = out
        return self.salaires_3_derniers_mois_indiv1_

    def salaires_3_derniers_mois_menage(self, s: Solver) -> Montant:
        if self.salaires_3_derniers_mois_menage_ is None:
            out = Montant.anonyme(s, "salaires_3_derniers_mois_menage")
            s.add(Implies(
                self.composition == Composition.celibataire(),
                out == self.salaires_3_derniers_mois_indiv0()
            ))
            s.add(Implies(
                Or(self.composition == Composition.concubinage(),
                self.composition == Composition.couple()),
                out == self.salaires_3_derniers_mois_indiv0() +
                self.salaires_3_derniers_mois_indiv1(s)
            ))
            self.salaires_3_derniers_mois_menage_ = out
        return self.salaires_3_derniers_mois_menage_

    def nb_enfants_plus_3_ans_moins_20_ans(self, s: Solver) -> BitVec:
        if self.nb_enfants_plus_3_ans_moins_20_ans_ is None:
            out = BitVec("nb_enfants_moins3_plus20_menage{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_enfants_plus_3_ans_moins_20_ans(s) + self.f2.nb_enfants_plus_3_ans_moins_20_ans(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_enfants_plus_3_ans_moins_20_ans(s)
            ))
            self.nb_enfants_plus_3_ans_moins_20_ans_ = out
        return self.nb_enfants_plus_3_ans_moins_20_ans_

    def nb_enfants_moins_3_ans_strict(self, s: Solver) -> BitVec:
        if self.nb_enfants_moins_3_ans_strict_ is None:
            out = BitVec("nb_enfants_moins_3_ans_strict{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_enfants_moins_3_ans_strict(s) + self.f2.nb_enfants_moins_3_ans_strict(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_enfants_moins_3_ans_strict(s)
            ))
            self.nb_enfants_moins_3_ans_strict_ = out
        return self.nb_enfants_moins_3_ans_strict_

    def nb_enfants_moins_20_ans_strict(self, s: Solver) -> BitVec:
        if self.nb_enfants_moins_20_ans_strict_ is None:
            out = BitVec("nb_enfants_moins_20_ans_strict{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_enfants_moins_20_ans_strict(s) + self.f2.nb_enfants_moins_20_ans_strict(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_enfants_moins_20_ans_strict(s)
            ))
            self.nb_enfants_moins_20_ans_strict_ = out
        return self.nb_enfants_moins_20_ans_strict_

    def nb_enfants_plus_14_ans_moins_20_ans_strict(self, s: Solver) -> BitVec:
        if self.nb_enfants_plus_14_ans_moins_20_ans_strict_ is None:
            out = BitVec("nb_enfants_plus_14_ans_moins_20_ans_strict{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_enfants_plus_14_ans_moins_20_ans_strict(s) + self.f2.nb_enfants_plus_14_ans_moins_20_ans_strict(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_enfants_plus_14_ans_moins_20_ans_strict(s)
            ))
            self.nb_enfants_plus_14_ans_moins_20_ans_strict_ = out
        return self.nb_enfants_plus_14_ans_moins_20_ans_strict_

    def nb_enfants_20_ans(self, s: Solver) -> BitVec:
        if self.nb_enfants_20_ans_ is None:
            out = BitVec("nb_enfants_20_ans{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_enfants_20_ans(s) + self.f2.nb_enfants_20_ans(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_enfants_20_ans(s)
            ))
            self.nb_enfants_20_ans_ = out
        return self.nb_enfants_20_ans_

    def nb_enfants_au_college(self, s: Solver) -> BitVec:
        if self.nb_enfants_au_college_ is None:
            out = BitVec("nb_enfants_au_college{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_enfants_au_college(s) + self.f2.nb_enfants_au_college(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_enfants_au_college(s)
            ))
            self.nb_enfants_au_college_ = out
        return self.nb_enfants_au_college_

    def nb_enfants_au_lycee(self, s: Solver) -> BitVec:
        if self.nb_enfants_au_lycee_ is None:
            out = BitVec("nb_enfants_au_lycee{}".format(Menage.counter), repr_bits)
            Menage.counter += 1
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.nb_enfants_au_lycee(s) + self.f2.nb_enfants_au_lycee(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.nb_enfants_au_lycee(s)
            ))
            self.nb_enfants_au_lycee_ = out
        return self.nb_enfants_au_lycee_

    def revenu_salaires(self, s: Solver) -> Montant:
        if self.revenu_salaires_ is None:
            out = Montant.anonyme(s, "rev_salaires")
            s.add(Implies(
                self.composition == Composition.concubinage(),
                out == self.f1.revenu_salaires(s) + self.f2.revenu_salaires(s)
            ))
            s.add(Implies(
                self.composition != Composition.concubinage(),
                out == self.f1.revenu_salaires(s)
            ))
            self.revenu_salaires_ = out
        return self.revenu_salaires_

    def revenu_fiscal_reference(self, s: Solver) -> Montant:
        if self.revenu_fiscal_reference_ is None:
            self.revenu_fiscal_reference_ = self.revenu_salaires(s) % Taux.pourcent(90)
        return self.revenu_fiscal_reference_

    def salaire_constant(self, s: Solver) -> None:
        self.f1.salaire_constant(s)
        self.f2.salaire_constant(s)

    def augmentation_salaire_premier_individu(self, m: Montant) -> 'Menage':
        return Menage(
            self.f1.augmentation_salaire_premier_individu(m),
            self.f2,
            self.composition,
            self.logement
        )

    def mariage_ou_pacs(self, s: Solver) -> 'Menage':
        s.add(self.composition == Composition.concubinage())
        s.add(
            self.f1.nb_personnes_a_charge + self.f2.nb_personnes_a_charge <=
            FoyerFiscal.max_a_charge
        )
        for i in range(FoyerFiscal.max_a_charge):
            # Cette méthode peu propre de concaténation marche car les enfants
            # dans f2 et f1 ont tous des id différents
            s.add(Implies(
                i < self.f2.nb_personnes_a_charge,
                Or([
                    And(
                        j >= self.f1.nb_personnes_a_charge,
                        j - self.f1.nb_personnes_a_charge < self.f2.nb_personnes_a_charge,
                        (self.f2.personnes_a_charge[i] ==
                        self.f1.personnes_a_charge[j])
                    )
                    for j in range(FoyerFiscal.max_a_charge)
                ])
            ))
        s.add(self.f1.individus[1] == self.f2.individus[0])
        new_f1 = FoyerFiscal(
            Composition.couple(),
            BoolVal(False),  # type: ignore
            BoolVal(False),  # type: ignore
            self.f1.individus,
            self.f1.personnes_a_charge,
            self.f1.nb_personnes_a_charge + self.f2.nb_personnes_a_charge
        )
        new_f1.ok(s)
        out = Menage(
            new_f1,
            self.f2,
            Composition.couple(),
            self.logement
        )
        out.assert_ok(s)
        return out

    def nouvel_enfant(self, s: Solver, age: Optional[int], changement_logement: bool) -> 'Menage':
        if changement_logement:
            new_logement = Logement.variable("new_logement{}".format(Menage.counter), s)
            Menage.counter += 1

        return Menage(
            self.f1.nouvel_enfant(s, age),
            self.f2,
            self.composition,
            new_logement if changement_logement else self.logement
        )

    def nouvelle_repartition_revenus(self, s: Solver) -> 'Menage':
        out = Menage(
            self.f1.same_foyer_fiscal_different_revenue("nouvelle_repartition_revenus{}".format(Menage.counter), s),
            self.f2.same_foyer_fiscal_different_revenue("nouvelle_repartition_revenus{}".format(Menage.counter + 1), s),
            self.composition,
            self.logement
        )
        Menage.counter += 2
        s.add(self.revenu_salaires(s) == out.revenu_salaires(s))
        return out

    def evolution_libre(
        self,
        max_variation_revenus: int,
        max_variation_personnes_charge: int,
        max_variation_loyer: int,
        s: Solver
    ) -> 'Menage':
        new_f1 = self.f1.variation_revenus_salaires(max_variation_revenus, s)
        new_f2 = self.f2.variation_revenus_salaires(max_variation_revenus, s)
        new_f1 = new_f1.variation_nb_personnes_charge(s, max_variation_personnes_charge)
        old_f1_revenue = self.f1.revenu_salaires(s)
        new_f1_revenue = new_f1.revenu_salaires(s)
        old_f2_revenue = self.f2.revenu_salaires(s)
        new_f2_revenue = new_f2.revenu_salaires(s)
        f1_revenue_variation = Montant.anonyme(s, "_variation_f1_{}".format(Menage.counter))
        f2_revenue_variation = Montant.anonyme(s, "_variation_f2_{}".format(Menage.counter))
        Menage.counter += 2
        s.add(Implies(
            new_f1_revenue - old_f1_revenue >= Montant.euros(0, s),
            f1_revenue_variation == new_f1_revenue - old_f1_revenue
        ))
        s.add(Implies(
            new_f1_revenue - old_f1_revenue < Montant.euros(0, s),
            f1_revenue_variation == old_f1_revenue - new_f1_revenue
        ))
        s.add(Implies(
            Or(self.composition == Composition.celibataire(),
            self.composition == Composition.couple()),
            f2_revenue_variation == Montant.euros(0, s)
        ))
        s.add(Implies(
            And(new_f2_revenue - old_f2_revenue >= Montant.euros(0, s),
            self.composition == Composition.concubinage()),
            f2_revenue_variation == new_f2_revenue - old_f2_revenue
        ))
        s.add(Implies(
            And(new_f2_revenue - old_f2_revenue < Montant.euros(0, s),
            self.composition == Composition.concubinage()),
            f2_revenue_variation == old_f2_revenue - new_f2_revenue
        ))
        s.add(f1_revenue_variation + f2_revenue_variation <= Montant.euros(max_variation_revenus, s))
        out = Menage(
            new_f1,
            new_f2,
            self.composition,
            self.logement.variation_mensualite(max_variation_loyer, s)
        )
        return out

    def __str__(self) -> str:
        if self.composition == Composition.concubinage():
            return (
                "Ménage en concubinage composé de deux foyers fiscaux.\n\n" +
                "Logement du ménage :\n\n{}\n\n".format(self.logement) +
                "Voici le premier foyer fiscal :\n\n" +
                self.f1.__str__() +
                "\n\nEt le second:\n\n" +
                self.f2.__str__()
            )
        else:
            return "Logement du ménage :\n\n{}\n\nFoyer fiscal du ménage :\n\n".format(self.logement) + self.f1.__str__()
