# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from typing import Union, Any, List

# HYPOTHÈSE: tout les montants calculés ont moins de 2^34 ~ 2*10^11 centimes ~ 20 M€
repr_bits: int = 34


class Taux:
    counter: int = 0

    def __init__(self, num: Union[int, BitVec, BitVecVal], denom: Union[int, BitVec, BitVecVal]):
        self.num = num
        self.denom = denom

    @staticmethod
    def pourcent(pourcent: Union[int, BitVec, BitVecVal]) -> 'Taux':
        return Taux(pourcent, 100)

    @staticmethod
    def pourmille(pourmille: Union[int, BitVec, BitVecVal]) -> 'Taux':
        return Taux(pourmille, 1000)

    def evaluate(self, s: Solver) -> 'Taux':
        return Taux(
            s.model().evaluate(self.num),
            s.model().evaluate(self.denom)
        )

    def __eq__(self, rhs: Any) -> bool:
        if not isinstance(rhs, Taux):
            raise TypeError("should be a Taux")
        return self.num == rhs.num and self.denom == rhs.denom

    def __str__(self) -> str:
        return "{}/{}".format(self.num, self.denom)


class Montant:
    counter: int = 0

    @staticmethod
    def smic_mi_temps_net(s: Solver) -> 'Montant':
        return Montant.centimes(78312, s)

    @staticmethod
    def bitvec_sat_sub(a: Union[BitVec, int, BitVecVal], b: int, s: Solver) -> BitVec:
        out = BitVec("i{}".format(Montant.counter), repr_bits)
        Montant.counter += 1
        s.add(Implies(a - b > 0, out == a - b))
        s.add(Implies(Not(a - b > 0), out == 0))
        return out

    def __init__(self, valeur: Union[int, BitVec, BitVecVal], s: Solver):
        self.valeur = valeur
        self.solver = s

    def evaluate(self, s: Solver) -> 'Montant':
        try:
            return Montant(s.model().evaluate(self.valeur, model_completion=True).as_long(), s)
        except AttributeError:
            return Montant(self.valeur, s)

    @staticmethod
    def euros(euros: int, s: Solver) -> 'Montant':
        return Montant(BitVecVal(euros * 100, repr_bits), s)

    @staticmethod
    def centimes(centimes: int, s: Solver) -> 'Montant':
        return Montant(BitVecVal(centimes, repr_bits), s)

    @staticmethod
    def anonyme(s: Solver, help: str) -> 'Montant':
        var = BitVec("m{}_{}".format(Montant.counter, help), repr_bits)
        s.add(0 <= var)
        Montant.counter += 1
        out = Montant(var, s)
        return out

    def __add__(self, rhs: 'Montant') -> 'Montant':
        # self.solver.add(BVAddNoOverflow(self.valeur, rhs.valeur, True))
        # self.solver.add(BVAddNoUnderflow(self.valeur, rhs.valeur))
        result = Montant(self.valeur + rhs.valeur, self.solver)
        return result

    # This is a saturating sub
    def sub(self, rhs: 'Montant') -> 'Montant':
        result = Montant.anonyme(self.solver, "sub")
        # self.solver.add(BVSubNoOverflow(self.valeur, rhs.valeur))
        self.solver.add(Implies(self.valeur < rhs.valeur, result.valeur == 0))
        self.solver.add(Implies(
            self.valeur >= rhs.valeur,
            result.valeur == self.valeur - rhs.valeur
        ))
        return result

    def __sub__(self, rhs: 'Montant') -> 'Montant':
        # if isinstance(self.valeur, BitVecRef):  # type:ignore
            # self.solver.add(BVSubNoOverflow(self.valeur, rhs.valeur))
            # self.solver.add(BVSubNoUnderflow(self.valeur, rhs.valeur, True))
        result = Montant(self.valeur - rhs.valeur, self.solver)
        return result

    def __truediv__(self, rhs: 'Montant') -> 'Montant':
        if isinstance(self.valeur, int) and isinstance(rhs.valeur, int):
            return Montant(self.valeur // rhs.valeur, self.solver)
        else:
            self.solver.add(BVSDivNoOverflow(self.valeur, rhs.valeur))
            return Montant(self.valeur / rhs.valeur, self.solver)  # type: ignore

    def __floordiv__(self, rhs: Union[int, BitVec, BitVecVal]) -> 'Montant':

        if isinstance(self.valeur, int) and isinstance(rhs, int):
            new_val = BitVecVal(self.valeur, repr_bits) / BitVecVal(rhs, repr_bits)
        elif isinstance(self.valeur, int):
            new_val = BitVecVal(self.valeur, repr_bits) / rhs
        else:
            self.solver.add(BVSDivNoOverflow(self.valeur, rhs))
            new_val = self.valeur / rhs
        return Montant(new_val, self.solver)

    def __lt__(self, rhs: 'Montant') -> bool:
        return self.valeur < rhs.valeur

    def __le__(self, rhs: 'Montant') -> bool:
        return self.valeur <= rhs.valeur

    def __gt__(self, rhs: 'Montant') -> bool:
        return self.valeur > rhs.valeur

    def __ge__(self, rhs: 'Montant') -> bool:
        return self.valeur >= rhs.valeur

    def __eq__(self, rhs: Any) -> bool:
        if not isinstance(rhs, Montant):
            return False
        else:
            return self.valeur == rhs.valeur

    def __ne__(self, rhs: Any) -> bool:
        if not isinstance(rhs, Montant):
            return False
        else:
            return self.valeur != rhs.valeur

    def __mod__(self, rhs: Taux) -> 'Montant':
        # self.solver.add(BVMulNoOverflow(self.valeur, rhs.num, True))
        # self.solver.add(BVMulNoUnderflow(self.valeur, rhs.num))
        # self.solver.add(BVSDivNoOverflow(self.valeur, rhs.denom))
        m = Montant((self.valeur * rhs.num) / rhs.denom, self.solver)  # type: ignore
        return m

    def __mul__(self, rhs: Union[int, BitVec, BitVecVal]) -> 'Montant':
        if isinstance(rhs, Montant):
            # self.solver.add(BVMulNoOverflow(self.valeur, rhs.valeur, True))
            # self.solver.add(BVMulNoUnderflow(self.valeur, rhs.valeur))
            m = Montant(self.valeur * rhs.valeur, self.solver)
        else:
            # self.solver.add(BVMulNoOverflow(self.valeur, rhs, True))
            # self.solver.add(BVMulNoUnderflow(self.valeur, rhs))
            m = Montant(self.valeur * rhs, self.solver)
        return m

    def round_to_euro(self) -> 'Montant':
        result = Montant.anonyme(self.solver, "rounding")
        self.solver.add(Implies(
            self.valeur % 100 < 50, result.valeur == self.valeur - (self.valeur % 100)
        ))
        self.solver.add(Implies(
            self.valeur % 100 >= 50, result.valeur == self.valeur + (100 - (self.valeur % 100))
        ))
        return result

    def to_float(self) -> Union[BitVec, BitVecVal, int]:
        valeur_reelle = self.valeur
        # The following magic is necessary because Z3 returns a signed integer
        # on `repr_bits` bits and it is not correctly interpreted by Python
        # when it is negative
        bit_mask = 0x1 << (repr_bits - 1)
        valeur_reelle_sign = (valeur_reelle & bit_mask) >> (repr_bits - 1)  # type:ignore
        valeur_reelle_val = valeur_reelle ^ bit_mask  # type:ignore
        val_mask = ((1 << (repr_bits - 1)) - 1)
        if valeur_reelle_sign:
            valeur_reelle = (valeur_reelle_val ^ val_mask) + 1
            valeur_reelle = - valeur_reelle
        return float(valeur_reelle) / 100  # type: ignore

    def __str__(self) -> str:
        valeur_reelle = self.valeur
        # The following magic is necessary because Z3 returns a signed integer
        # on `repr_bits` bits and it is not correctly interpreted by Python
        # when it is negative
        bit_mask = 0x1 << (repr_bits - 1)
        valeur_reelle_sign = (valeur_reelle & bit_mask) >> (repr_bits - 1)  # type:ignore
        valeur_reelle_val = valeur_reelle ^ bit_mask  # type:ignore
        val_mask = ((1 << (repr_bits - 1)) - 1)
        if valeur_reelle_sign:
            valeur_reelle = (valeur_reelle_val ^ val_mask) + 1
            sign_str = "- "
        else:
            sign_str = ""
        if valeur_reelle >= 100000000000:
            return "{}{} {:03d} {:03d} {:03d},{:02d} €".format(  # type: ignore
                sign_str,
                valeur_reelle // 100000000,  # type: ignore
                (valeur_reelle % 100000000000) // 100000000,  # type: ignore
                (valeur_reelle % 100000000) // 100000,  # type: ignore
                (valeur_reelle % 100000) // 100,  # type: ignore
                valeur_reelle % 100
            )
        if valeur_reelle >= 100000000:
            return "{}{} {:03d} {:03d},{:02d} €".format(  # type: ignore
                sign_str,
                valeur_reelle // 100000000,  # type: ignore
                (valeur_reelle % 100000000) // 100000,  # type: ignore
                (valeur_reelle % 100000) // 100,  # type: ignore
                valeur_reelle % 100
            )

        if valeur_reelle >= 100000:
            return "{}{} {:03d},{:02d} €".format(  # type: ignore
                sign_str,
                valeur_reelle // 100000,  # type: ignore
                (valeur_reelle % 100000) // 100,  # type: ignore
                valeur_reelle % 100
            )

        return "{}{},{:02d} €".format(sign_str, valeur_reelle // 100, valeur_reelle % 100)  # type: ignore

    def __repr__(self) -> Any:
        return Montant.__str__(self)
