# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from montant import *
from individu import *
from foyerfiscal import *
from solveur import *
from allocations_familiales2019 import *
from APL2019 import *


class PrimeActivite2019:
    counter: int = 0

    @staticmethod
    def montant_forfaitaire(s: Solver) -> Montant:
        return Montant.centimes(55151, s)

    def __init__(self, baf: AllocationsFamiliales2019, bapl: APL2019):
        self.menage = baf.menage
        self.baf = baf
        self.bapl = bapl

    def montant_forfaitaire_majore_normal(self, s: Solver) -> Montant:
        npc = self.menage.nb_personnes_a_charge(s)
        majoration = BitVec("prime_activite_majoration{}".format(PrimeActivite2019.counter), repr_bits)
        PrimeActivite2019.counter += 1
        s.add(Implies(
            And(npc == 0, self.menage.composition == Composition.celibataire()),
            majoration == 100
        ))
        s.add(Implies(
            And(npc == 1, self.menage.composition == Composition.celibataire()),
            majoration == 150
        ))
        s.add(Implies(
            And(npc == 2, self.menage.composition == Composition.celibataire()),
            majoration == 180
        ))
        s.add(Implies(
            And(npc > 2, self.menage.composition == Composition.celibataire()),
            majoration == 180 + 40 * (npc - 2)
        ))
        s.add(Implies(
            And(npc == 0, Or(self.menage.composition == Composition.couple(),
            self.menage.composition == Composition.concubinage())),
            majoration == 150
        ))
        s.add(Implies(
            And(npc == 1, Or(self.menage.composition == Composition.couple(),
            self.menage.composition == Composition.concubinage())),
            majoration == 180
        ))
        s.add(Implies(
            And(npc == 2, Or(self.menage.composition == Composition.couple(),
            self.menage.composition == Composition.concubinage())),
            majoration == 210
        ))
        s.add(Implies(
            And(npc > 2, Or(self.menage.composition == Composition.couple(),
            self.menage.composition == Composition.concubinage())),
            majoration == 210 + 40 * (npc - 2)
        ))

        return PrimeActivite2019.montant_forfaitaire(s) % Taux.pourcent(majoration)

    def montant_forfaitaire_majore_parent_isole(self, s: Solver) -> Montant:
        majoration = BitVec("prime_activite_majoration{}".format(PrimeActivite2019.counter), repr_bits)
        PrimeActivite2019.counter += 1
        s.add(Implies(self.menage.nb_personnes_a_charge(s) == 0, majoration == 1284))
        s.add(Implies(
            self.menage.nb_personnes_a_charge(s) >= 1,
            majoration == 1284 + 428 * self.menage.nb_personnes_a_charge(s)
        ))
        return PrimeActivite2019.montant_forfaitaire(s) % Taux.pourmille(majoration)

    def montant_forfaitaire_majore(self, s: Solver) -> Montant:
        isole = self.montant_forfaitaire_majore_parent_isole(s)
        normal = self.montant_forfaitaire_majore_normal(s)
        out = Montant.anonyme(s, "montant_forfaitaire_majore")
        maj_cond = And(
            Or(self.menage.f1.parent_isole, self.menage.f1.veuf),
            self.menage.nb_personnes_a_charge(s) > 0
        )
        s.add(Implies(
            maj_cond,
            out == isole
        ))
        s.add(Implies(
            Not(maj_cond),
            out == normal
        ))
        return out

    def bonifications_individuelles_fois_3(self, s: Solver, indiv: int) -> Montant:
        if not (indiv in [0, 1]):
            raise TypeError("can only compute prime d'activité for members of foyer")
        salaire_min = Montant.centimes(59177, s)
        salaire_max = Montant.centimes(120360, s)
        if indiv == 0:
            salaire_3_mois = self.menage.salaires_3_derniers_mois_indiv0()
        else:
            salaire_3_mois = self.menage.salaires_3_derniers_mois_indiv1(s)

        # The fraction 107 // 414 approximates the real value of the pente 0,258454146
        # with a small numerator value so that the multiplication does not overflow
        affine = Montant.centimes(236, s) * 3 + ((salaire_3_mois.sub(salaire_min * 3) * 107) // 414)
        out = Montant.anonyme(s, "bonifications_individuelles")
        s.add(Implies(
            salaire_3_mois <= salaire_min * 3,
            out == Montant.euros(0, s) * 3
        ))
        s.add(Implies(
            salaire_3_mois > salaire_max * 3,
            out == Montant.centimes(16049, s) * 3
        ))
        s.add(Implies(
            And(salaire_min * 3 < salaire_3_mois,
            salaire_3_mois <= salaire_max * 3),
            out == affine
        ))
        return out

    def forfait_logement_trois_mois(self, s: Solver) -> Montant:
        out = Montant.anonyme(s, "forfait_logement")
        nb_personnes = self.menage.nombre_personnes(s)
        s.add(Implies(nb_personnes == 1, out == Montant.centimes(6618, s) * 3))
        s.add(Implies(nb_personnes == 2, out == Montant.centimes(13236, s) * 3))
        s.add(Implies(nb_personnes >= 3, out == Montant.centimes(16380, s) * 3))
        return out

    def ressources_prises_en_compte_fois_3(self, s: Solver) -> Montant:
        # HYPOTHÈSE: ressources prises en compte pour la PA: salaires, allocations familiales et APL ou forfait logement
        salaires_3_mois = self.menage.salaires_3_derniers_mois_menage(s)
        [_, _, _, _, af] = self.baf.calcul_avec_etapes(s)
        logement = Montant.anonyme(s, "ressources_logement")
        s.add(Implies(
            self.menage.logement.is_herbergement_gratuit(),
            logement == self.forfait_logement_trois_mois(s)
        ))
        s.add(Implies(
            self.menage.logement.is_location(),
            logement == self.bapl.montant(s) * 3
        ))
        return salaires_3_mois + af * 3 + logement

    def montant(self, s: Solver) -> Montant:
        salaires_3_mois = self.menage.salaires_3_derniers_mois_menage(s)
        bon0 = self.bonifications_individuelles_fois_3(s, 0)
        bon1 = self.bonifications_individuelles_fois_3(s, 1)
        bon = Montant.anonyme(s, "bonifications_individuelles_fois_3_menage")
        s.add(Implies(
            self.menage.composition == Composition.celibataire(),
            bon == bon0
        ))
        s.add(Implies(
            Or(self.menage.composition == Composition.couple(),
            self.menage.composition == Composition.concubinage()),
            bon == bon0 + bon1
        ))
        mfm = self.montant_forfaitaire_majore(s)
        ress = self.ressources_prises_en_compte_fois_3(s)
        pa1 = (
            (
                (mfm * 3) +
                (salaires_3_mois % Taux.pourcent(61)) +
                bon
            ).sub(ress)
        ) // 3
        pa2 = (mfm * 3 - ress) // 3
        pa = Montant.anonyme(s, "prime_activite")
        s.add(Implies(
            pa2 <= Montant.euros(0, s),
            pa == pa1
        ))
        s.add(Implies(
            pa2 > Montant.euros(0, s),
            pa == pa1 - pa2
        ))
        out = Montant.anonyme(s, "prime_activite_final")
        not_given_condition = Or(
            pa < Montant.euros(15, s),
            salaires_3_mois <
            PrimeActivite2019.montant_forfaitaire(s) * 3
        )
        s.add(Implies(not_given_condition, out == Montant.euros(0, s)))
        s.add(Implies(Not(not_given_condition), out == pa))
        return out.round_to_euro()
