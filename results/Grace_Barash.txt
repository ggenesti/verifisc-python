Fichier généré par le prototype Python de Verifisc (version b'17138c3') le 2019-06-14T09:56:45.355973

Recherche d'un taux marginal anormalement élevé.
Voici la valeur des paramètres de la requête :

| Paramètre                                                              | Valeur                       |
|:-----------------------------------------------------------------------|:-----------------------------|
|                                                                        | 70%                          |
| Nombre maximal d'invalides à charge                                    | 0                            |
| Nombre maximal de personnes à charge                                   | 2                            |
| Revenus mensuel salarial minimal                                       | 0,00 €                       |
| Revenus mensuel salarial maximal                                       | 2 500,00 €                   |
| Ratio loyer/salaire minimal                                            | 20%                          |
| Ratio loyer/salaire maximal                                            | 40%                          |
| Éviter le concubinage                                                  | True                         |
| Éviter l'hébergement gratuit                                           | True                         |
| Augmentation mensuelle du premier individu                             | 250                          |
| Éviter le seuil de non prélèvement de l'IR                             | False                        |
| Éviter le seuil de non distribution de la prime d'activité             | False                        |
| Éviter le plafond de non distribution de l'allocation rentrée scolaire | False                        |
| Éviter tout effet de seuil du complément familial                      | False                        |
| Éviter tout effet de seuil de la PAJE                                  | False                        |
| Éviter le seuil de non distribution des bourses lycéennes              | False                        |
| Éviter le seuil de non distribution des bourses collégiennes           | False                        |
| Dispositions prises en compte                                          | APL, BL, BC, IR, PA, ARS, AF |

Résultat trouvé en 8229.86 secondes avec 5970.86 Mo de mémoire RAM consommée.

Possible ! Voici un ménage remplissant les conditions :

Logement du ménage :

| Caractéristique   | Valeur   |
|:------------------|:---------|
| Type de logement  | Location |
| Loyer mensuel     | 892,43 € |
| Zone              | II       |

Foyer fiscal du ménage :

| Caractéristique              | Valeur      |
|:-----------------------------|:------------|
| Situation familiale          | célibataire |
| Nombre de personnes à charge | 1           |
| Parent isolé                 | True        |
| Veuf ou veuve                | False       |

Individus du foyer :

| Caractéristique   | Valeur pour l'individu 1   |
|:------------------|:---------------------------|
| En activité       | True                       |
| Salaire mensuel   | 2 278,24 €                 |

Personnes à charge:

| Caractéristique   | Valeur pour l'individu 1   |
|:------------------|:---------------------------|
| En activité       | False                      |
| Scolarisé         | True                       |
| Invalide          | 15                         |
| Âge               | False                      |

Maintenant, les montants calculés avant et après:

| Montant                           | Valeur avant   | Valeur après   | Delta      |
|:----------------------------------|:---------------|:---------------|:-----------|
| Salaire annuel                    | 27 338,88 €    | 30 338,88 €    | 3 000,00 € |
| Revenu fiscal de référence        | 24 605,00 €    | 27 305,00 €    | 2 700,00 € |
| Droits simples                    | 655,00 €       | 1 033,00 €     | 378,00 €   |
| Décote                            | 655,00 €       | 421,00 €       | - 234,00 € |
| RSCR                              | 0,00 €         | 79,00 €        | 79,00 €    |
| IR                                | 0,00 €         | 533,00 €       | 533,00 €   |
| Base allocations                  | 0,00 €         | 0,00 €         | 0,00 €     |
| Majoration allocations familiales | 0,00 €         | 0,00 €         | 0,00 €     |
| Complément familial               | 0,00 €         | 0,00 €         | 0,00 €     |
| PAJE                              | 0,00 €         | 0,00 €         | 0,00 €     |
| Allocations familiales            | 0,00 €         | 0,00 €         | 0,00 €     |
| Allocation rentrée scolaire       | 403,00 €       | 0,00 €         | - 403,00 € |
| Bourses collège                   | 0,00 €         | 0,00 €         | 0,00 €     |
| Bourses lycée                     | 0,00 €         | 0,00 €         | 0,00 €     |
| APL                               | 0,00 €         | 0,00 €         | 0,00 €     |
| Prime d'activité                  | 216,00 €       | 119,00 €       | - 97,00 €  |
| Net touché                        | 30 333,88 €    | 31 233,88 €    | 900,00 €   |

Et voici les autres variables demandées :

| Caractéristique                         | Valeur   |
|:----------------------------------------|:---------|
| Augmentation mensuelle des revenus      | 250,00 € |
| Changement mensuel après redistribution | 75,00 €  |
| Taux marginal cumulé                    | 70,0 %   |
