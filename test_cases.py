# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from montant import *
from menage import *
from IR import *
from solveur import *
from individu import *
from prime_activite import *
from allocations_familiales import *
from logement import *


def test_1() -> None:
        s = Solver()
        s.set("timeout", 1000 * 60)
        m = Menage.variable("menage1", s)
        s.add(m.composition == Composition.couple())
        m.f1.salaire_constant(s)
        s.add(m.f1.individus[0].salaires[0] == Montant.euros(1500, s))
        s.add(m.f1.individus[1].salaires[0] == Montant.euros(3000, s))
        s.add(m.f1.nb_personnes_a_charge == 2)
        s.add(m.f1.nb_invalides_a_charge(s) == 0)
        rfr = m.f1.revenu_fiscal_reference(s)
        ir = IRMenage(m)
        ir1 = ir.montant(s)

        c = s.check()
        ir_float = ir1.evaluate(s).to_float()
        assert ir_float == 2478


def test_2() -> None:
        s = Solver()
        s.set("timeout", 1000 * 60)
        f = FoyerFiscal.variable("foyer1", s)
        f.salaire_constant(s)
        s.add(f.composition == Composition.couple())
        s.add(f.individus[0].salaires[0] == Montant.euros(1500, s))
        s.add(f.individus[1].salaires[0] == Montant.euros(3000, s))
        s.add(f.nb_personnes_a_charge == 2)
        s.add(f.nb_invalides_a_charge(s) == 0)
        rfr = f.revenu_fiscal_reference(s)
        ir = IR(f)
        ir1 = ir.montant(s)

        c = s.check()
        ir_float = ir1.evaluate(s).to_float()
        assert ir_float == 2478


def test_3() -> None:
        s = Solver()
        s.set("timeout", 1000 * 30)
        f = FoyerFiscal.variable("foyer1", s)
        f.salaire_constant(s)
        s.add(f.composition == Composition.couple())
        s.add(f.individus[0].salaires[0] == Montant.euros(1200, s))
        s.add(f.individus[1].salaires[0] == Montant.euros(0, s))
        s.add(f.nb_personnes_a_charge == 2)
        s.add(f.nb_invalides_a_charge(s) == 0)
        ir = IR(f).montant(s)
        c = s.check()
        assert ir.evaluate(s) == Montant.centimes(0, s)


def test_4() -> None:
        s = Solver()
        s.set("timeout", 1000 * 30)
        f = FoyerFiscal.variable("foyer1", s)
        f.salaire_constant(s)
        s.add(f.composition == Composition.couple())
        s.add(f.individus[0].salaires[0] == Montant.centimes(176552, s))
        s.add(f.individus[1].salaires[0] == Montant.centimes(199222, s))
        s.add(f.nb_personnes_a_charge == 0)
        s.add(f.nb_invalides_a_charge(s) == 0)
        irf = IR(f)
        [rfr, droits_simples, decote, rscr, ir] = irf.calcul_avec_etapes(s)
        c = s.check()
        rfr
        assert rfr.evaluate(s).to_float() == 40584
        assert droits_simples.evaluate(s).to_float() == 2892
        assert decote.evaluate(s).to_float() == 0
        assert rscr.evaluate(s).to_float() == 210
        assert ir.evaluate(s).to_float() == 2682


def test_5() -> None:
        s = Solver()
        s.set("timeout", 1000 * 30)
        f = FoyerFiscal.variable("foyer1", s)
        f.salaire_constant(s)
        s.add(f.composition == Composition.celibataire())
        s.add(f.individus[0].salaires[0] == Montant.centimes(141838, s))
        s.add(f.nb_personnes_a_charge == 0)
        s.add(f.nb_invalides_a_charge(s) == 0)
        irf = IR(f)
        [rfr, droits_simples, decote, rscr, ir] = irf.calcul_avec_etapes(s)
        c = s.check()
        assert rfr.evaluate(s).to_float() == 15319
        assert droits_simples.evaluate(s).to_float() == 750
        assert decote.evaluate(s).to_float() == 633
        assert rscr.evaluate(s).to_float() == 23
        assert ir.evaluate(s).to_float() == 94


def test_6() -> None:
        s = Solver()
        s.set("timeout", 1000 * 30)
        f = FoyerFiscal.variable("foyer1", s)
        f.salaire_constant(s)
        s.add(f.composition == Composition.couple())
        s.add(f.individus[0].salaires[0] == Montant.centimes(181456, s))
        s.add(f.individus[1].salaires[0] == Montant.centimes(231179, s))
        s.add(f.nb_personnes_a_charge == 2)
        s.add(f.nb_invalides_a_charge(s) == 0)
        s.add(f.personnes_a_charge[0].age == 10)
        s.add(f.personnes_a_charge[1].age == 1)
        irf = IR(f)
        [rfr, droits_simples, decote, rscr, ir] = irf.calcul_avec_etapes(s)
        c = s.check()
        assert rfr.evaluate(s).to_float() == 44564
        assert droits_simples.evaluate(s).to_float() == 2054
        assert decote.evaluate(s).to_float() == 429
        assert rscr.evaluate(s).to_float() == 325
        assert ir.evaluate(s).to_float() == 1300


def test_7() -> None:
        s = Solver()
        s.set("timeout", 1000 * 60)
        m = Menage.variable("menage1", s)
        m.salaire_constant(s)
        s.add(m.composition == Composition.celibataire())
        s.add(m.f1.individus[0].salaires[0] == Montant.centimes(141838, s))
        s.add(m.f1.nb_personnes_a_charge == 0)
        baf = AllocationsFamiliales(m)
        [afbase, afmajoration, afcf, afpaje, af] = baf.calcul_avec_etapes(s)
        nb20 = m.nb_enfants_moins_20_ans_strict(s)
        c = s.check()
        assert afbase.evaluate(s).to_float() == 0
        assert afmajoration.evaluate(s).to_float() == 0
        assert afcf.evaluate(s).to_float() == 0
        assert afpaje.evaluate(s).to_float() == 0
        assert af.evaluate(s).to_float() == 0


def test_8() -> None:
    s = Solver()
    s.set("timeout", 1000 * 60)
    m = Menage.variable("menage", s)
    m.salaire_constant(s)
    s.add(m.composition == Composition.couple())
    s.add(m.f1.individus[0].salaires[0] == Montant.centimes(107223, s))
    s.add(m.f1.individus[1].salaires[0] == Montant.centimes(0, s))
    s.add(m.f1.nb_personnes_a_charge == 3)
    s.add(m.f1.personnes_a_charge[0].age == 11)
    s.add(m.f1.personnes_a_charge[1].age == 11)
    s.add(m.f1.personnes_a_charge[2].age == 11)
    s.add(m.f1.personnes_a_charge[0].scolarise)
    s.add(m.f1.personnes_a_charge[1].scolarise)
    s.add(m.f1.personnes_a_charge[2].scolarise)
    s.add(Not(m.f1.personnes_a_charge[0].invalide))
    s.add(m.f1.personnes_a_charge[1].invalide)
    s.add(Not(m.f1.personnes_a_charge[2].invalide))
    s.add(m.logement.is_herbergement_gratuit())
    baf = AllocationsFamiliales(m)
    bapl = APL(m)
    bpa = PrimeActivite(baf, bapl)
    pa = bpa.montant(s)
    [afbase, afmajoration, afcf, afpaje, af] = baf.calcul_avec_etapes(s)
    amrs = baf.allocation_rentree_scolaire(s)
    ambc = baf.bourses_college_trimestrielles(s) * 3
    ambl = baf.bourses_lycee_annuelles(s)
    c = s.check()
    assert afbase.evaluate(s).to_float() == 302
    assert afmajoration.evaluate(s).to_float() == 0
    assert afcf.evaluate(s).to_float() == 258
    assert afpaje.evaluate(s).to_float() == 0
    assert amrs.evaluate(s).to_float() == 1170
    assert ambc.evaluate(s).to_float() == 315
    assert ambl.evaluate(s).to_float() == 0
    assert pa.evaluate(s).to_float() == 363  # CAF gives 362


def test_9() -> None:
    s = Solver()
    s.set("timeout", 1000 * 60)
    m = Menage.variable("menage", s)
    m.salaire_constant(s)
    s.add(m.composition == Composition.couple())
    s.add(m.f1.individus[0].salaires[0] == Montant.centimes(84144, s))
    s.add(m.f1.individus[1].salaires[0] == Montant.centimes(120688, s))
    s.add(m.f1.nb_personnes_a_charge == 4)
    s.add(m.f1.personnes_a_charge[0].age == 20)
    s.add(m.f1.personnes_a_charge[1].age == 20)
    s.add(m.f1.personnes_a_charge[2].age == 20)
    s.add(m.f1.personnes_a_charge[3].age == 19)
    s.add(Not(m.f1.personnes_a_charge[0].invalide))
    s.add(Not(m.f1.personnes_a_charge[1].invalide))
    s.add(Not(m.f1.personnes_a_charge[2].invalide))
    s.add(Not(m.f1.personnes_a_charge[3].invalide))
    s.add(m.logement.is_herbergement_gratuit())
    baf = AllocationsFamiliales(m)
    bapl = APL(m)
    bpa = PrimeActivite(baf, bapl)
    pa = bpa.montant(s)
    [afbase, afmajoration, afcf, afpaje, af] = baf.calcul_avec_etapes(s)
    amrs = baf.allocation_rentree_scolaire(s)
    ambc = baf.bourses_college_trimestrielles(s) * 3
    ambl = baf.bourses_lycee_annuelles(s)
    c = s.check()
    assert afbase.evaluate(s).to_float() == 0
    assert afmajoration.evaluate(s).to_float() == 250
    assert afcf.evaluate(s).to_float() == 258
    assert afpaje.evaluate(s).to_float() == 0
    assert amrs.evaluate(s).to_float() == 0
    assert ambc.evaluate(s).to_float() == 0
    assert ambl.evaluate(s).to_float() == 0
    assert pa.evaluate(s).to_float() == 356  # CAF gives 352


def test_10() -> None:
    s = Solver()
    s.set("timeout", 1000 * 60)
    m = Menage.variable("menage", s)
    m.salaire_constant(s)
    s.add(m.composition == Composition.celibataire())
    s.add(m.f1.veuf)
    s.add(m.f1.individus[0].salaires[0] == Montant.centimes(142595, s))
    s.add(m.f1.nb_personnes_a_charge == 0)
    s.add(m.logement.is_herbergement_gratuit())
    bpa = PrimeActivite(AllocationsFamiliales(m), APL(m))
    pa = bpa.montant(s)
    c = s.check()
    assert pa.evaluate(s).to_float() == 90  # mes-aides.gouv.fr gives 85


def test_11() -> None:
    s = Solver()
    s.set("timeout", 1000 * 60)
    m = Menage.variable("menage", s)
    m.salaire_constant(s)
    s.add(m.composition == Composition.concubinage())
    s.add(m.f1.composition == Composition.concubinage())
    s.add(m.f2.composition == Composition.concubinage())
    s.add(m.f1.individus[0].salaires[0] == Montant.centimes(169467, s))
    s.add(m.f2.individus[0].salaires[0] == Montant.centimes(0, s))
    s.add(m.f1.nb_personnes_a_charge == 0)
    s.add(m.f2.nb_personnes_a_charge == 2)
    s.add(m.f2.personnes_a_charge[0].age == 7)
    s.add(m.f2.personnes_a_charge[1].age == 14)
    s.add(m.f2.personnes_a_charge[0].scolarise)
    s.add(m.f2.personnes_a_charge[1].scolarise)
    s.add(Not(m.f2.personnes_a_charge[0].invalide))
    s.add(Not(m.f2.personnes_a_charge[1].invalide))
    s.add(m.logement.is_herbergement_gratuit())
    baf = AllocationsFamiliales(m)
    bapl = APL(m)
    bpa = PrimeActivite(baf, bapl)
    pa = bpa.montant(s)
    [afbase, afmajoration, afcf, afpaje, af] = baf.calcul_avec_etapes(s)
    c = s.check()
    assert afbase.evaluate(s).to_float() == 132
    assert afmajoration.evaluate(s).to_float() == 0
    assert afcf.evaluate(s).to_float() == 0
    assert af.evaluate(s).to_float() == 132
    assert afpaje.evaluate(s).to_float() == 0
    assert pa.evaluate(s).to_float() == 362  # mes-aides.gouv.fr gives 360


def test_12() -> None:
    s = Solver()
    s.set("timeout", 1000 * 60)
    m = Menage.variable("menage", s)
    m.salaire_constant(s)
    s.add(m.composition == Composition.concubinage())
    s.add(m.f1.composition == Composition.concubinage())
    s.add(m.f2.composition == Composition.concubinage())
    s.add(m.f1.individus[0].salaires[0] == Montant.centimes(106481, s))
    s.add(m.f2.individus[0].salaires[0] == Montant.centimes(0, s))
    s.add(m.f1.nb_personnes_a_charge == 0)
    s.add(m.f2.nb_personnes_a_charge == 2)
    s.add(m.f2.personnes_a_charge[0].age == 7)
    s.add(m.f2.personnes_a_charge[1].age == 14)
    s.add(m.f2.personnes_a_charge[0].scolarise)
    s.add(m.f2.personnes_a_charge[1].scolarise)
    s.add(Not(m.f2.personnes_a_charge[0].invalide))
    s.add(Not(m.f2.personnes_a_charge[1].invalide))
    s.add(m.logement.mensualite == Montant.euros(425, s))
    s.add(m.logement.is_location())
    s.add(m.logement.zone == 3)
    bapl = APL(m)
    apl = bapl.montant(s)
    c = s.check()
    assert apl.evaluate(s).to_float() == 315


def test_13() -> None:
    s = Solver()
    s.set("timeout", 1000 * 60)
    m = Menage.variable("menage", s)
    m.salaire_constant(s)
    s.add(m.composition == Composition.celibataire())
    s.add(m.f1.parent_isole)
    s.add(m.f1.individus[0].salaires[0] == Montant.centimes(708202, s))
    s.add(m.f1.nb_personnes_a_charge == 3)
    s.add(m.f1.personnes_a_charge[0].age == 14)
    s.add(m.f1.personnes_a_charge[1].age == 14)
    s.add(m.f1.personnes_a_charge[2].age == 15)
    s.add(m.f1.personnes_a_charge[3].age == 19)
    s.add(Not(m.f1.personnes_a_charge[0].invalide))
    s.add(Not(m.f1.personnes_a_charge[1].invalide))
    s.add(Not(m.f1.personnes_a_charge[2].invalide))
    s.add(Not(m.f1.personnes_a_charge[3].invalide))
    s.add(m.logement.is_location())
    s.add(m.logement.mensualite == Montant.centimes(217295, s))
    s.add(m.logement.zone == 1)
    bir = IRMenage(m)
    ir = bir.montant(s)
    baf = AllocationsFamiliales(m)
    bapl = APL(m)
    apl = bapl.montant(s)
    bpa = PrimeActivite(baf, bapl)
    pa = bpa.montant(s)
    [afbase, afmajoration, afcf, afpaje, af] = baf.calcul_avec_etapes(s)
    amrs = baf.allocation_rentree_scolaire(s)
    ambc = baf.bourses_college_trimestrielles(s) * 3
    ambl = baf.bourses_lycee_annuelles(s)
    c = s.check()
    assert ir.evaluate(s).to_float() == 10683
    assert afbase.evaluate(s).to_float() == 302
    assert afmajoration.evaluate(s).to_float() == 198
    assert af.evaluate(s).to_float() == 250
    assert afcf.evaluate(s).to_float() == 0
    assert afpaje.evaluate(s).to_float() == 0
    assert amrs.evaluate(s).to_float() == 0
    assert ambc.evaluate(s).to_float() == 0
    assert ambl.evaluate(s).to_float() == 0
    assert pa.evaluate(s).to_float() == 0
    assert apl.evaluate(s).to_float() == 0


def test_14() -> None:
        s = Solver()
        s.set("timeout", 1000 * 60)
        f = FoyerFiscal.variable("foyer1", s)
        f.salaire_constant(s)
        s.add(f.composition == Composition.celibataire())
        s.add(f.individus[0].salaires[0] == Montant.centimes(174768, s))
        s.add(f.nb_personnes_a_charge == 0)
        rfr = f.revenu_fiscal_reference(s)
        ir = IR(f)
        [rfr, droits_simples, decote, ir] = ir.calcul_avec_etapes_2020(s)

        c = s.check()
        print(droits_simples.evaluate(s).to_float())
        print(decote.evaluate(s).to_float())
        ir_float = ir.evaluate(s).to_float()
        print(ir_float)
        assert ir_float == 631 #should be 631 ?
