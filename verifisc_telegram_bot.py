# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import telegram  # type: ignore


def post_message(msg: str) -> None:
    with open("TelegramBotToken.txt", "r") as f:
        token = f.read().rstrip("\n")
        bot = telegram.Bot(token)
        with open("TelegramChatID.txt", "r") as f:
            chat_id = int(f.read().rstrip("\n"))
            bot.send_message(chat_id=chat_id, text=msg)


def post_file(filename: str) -> None:
    with open("TelegramBotToken.txt", "r") as f:
        token = f.read().rstrip("\n")
        bot = telegram.Bot(token)
        data_file = open(filename, 'rb')
        with open("TelegramChatID.txt", "r") as f:
            chat_id = int(f.read().rstrip("\n"))
            bot.send_document(chat_id=chat_id, document=data_file)
            data_file.close()
