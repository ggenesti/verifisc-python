# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from typing import Union, Any


class Int:
    def __init__(self, name: str):
        ...

    def __add__(self, rhs: Any) -> Int:
        ...

    def __radd__(self, rhs: Any) -> Int:
        ...

    def __sub__(self, rhs: Any) -> Int:
        ...

    def __rsub__(self, rhs: Any) -> Int:
        ...

    def __mul__(self, rhs: Any) -> Int:
        ...

    def __rmul__(self, rhs: Any) -> Int:
        ...

    def __lt__(self, rhs: Any) -> bool:
        ...

    def __le__(self, rhs: Any) -> bool:
        ...

    def __gt__(self, rhs: Any) -> bool:
        ...

    def __ge__(self, rhs: Any) -> bool:
        ...


class BitVec:
    def __init__(self, name: str, precision: int):
        ...

    def __add__(self, rhs: Any) -> BitVec:
        ...

    def __radd__(self, rhs: Any) -> BitVec:
        ...

    def __sub__(self, rhs: Any) -> BitVec:
        ...

    def __rsub__(self, rhs: Any) -> BitVec:
        ...

    def __mul__(self, rhs: Any) -> BitVec:
        ...

    def __rmul__(self, rhs: Any) -> BitVec:
        ...

    def __truediv__(self, rhs: Any) -> BitVec:
        ...

    def __mod__(self, rhs: Any) -> BitVec:
        ...

    def __rtruediv__(self, rhs: Any) -> BitVec:
        ...

    def __lt__(self, rhs: Any) -> bool:
        ...

    def __le__(self, rhs: Any) -> bool:
        ...

    def __gt__(self, rhs: Any) -> bool:
        ...

    def __ge__(self, rhs: Any) -> bool:
        ...


class BitVecVal:
    def __init__(self, value: Any, precision: int):
        ...

    def __add__(self, rhs: Any) -> BitVec:
        ...

    def __radd__(self, rhs: Any) -> BitVec:
        ...

    def __sub__(self, rhs: Any) -> BitVec:
        ...

    def __rsub__(self, rhs: Any) -> BitVec:
        ...

    def __mul__(self, rhs: Any) -> BitVec:
        ...

    def __rmul__(self, rhs: Any) -> BitVec:
        ...

    def __mod__(self, rhs: Any) -> BitVec:
        ...

    def __truediv__(self, rhs: Any) -> BitVec:
        ...

    def __rtruediv__(self, rhs: Any) -> BitVec:
        ...

    def __lt__(self, rhs: Any) -> bool:
        ...

    def __le__(self, rhs: Any) -> bool:
        ...

    def __gt__(self, rhs: Any) -> bool:
        ...

    def __ge__(self, rhs: Any) -> bool:
        ...


class Real:
    def __init__(self, name: str):
        ...

    def __add__(self, rhs: Any) -> Real:
        ...

    def __radd__(self, rhs: Any) -> Real:
        ...

    def __sub__(self, rhs: Any) -> Real:
        ...

    def __rsub__(self, rhs: Any) -> Real:
        ...

    def __mul__(self, rhs: Any) -> Real:
        ...

    def __rmul__(self, rhs: Any) -> Real:
        ...

    def __truediv__(self, rhs: Any) -> Real:
        ...

    def __rtruediv__(self, rhs: Any) -> Real:
        ...

    def __lt__(self, rhs: Any) -> bool:
        ...

    def __le__(self, rhs: Any) -> bool:
        ...

    def __gt__(self, rhs: Any) -> bool:
        ...

    def __ge__(self, rhs: Any) -> bool:
        ...


class Bool:
    def __init__(self, name: str):
        ...


class RealNumRef:
    ...


class RatNumRef:
    def numerator(self) -> Any:
        ...

    def denominator(self) -> Any:
        ...


def Q(num: Any, denom: Any) -> RatNumRef:
    ...


def If(a: Any, b: Any, c: Any) -> Any:
    ...


class Model:
    def evaluate(self, e: Any, model_completion: bool=False) -> Any:
        ...


class Solver:
    def add(self, *constrafloats: Any) -> None:
        ...

    def check(self, *constrafloats: Any) -> Any:
        ...

    def model(self) -> Model:
        ...

    def unsat_core(self) -> Any:
        ...

    def to_smt2(self) -> Any:
        ...

    def reset(self) -> None:
        ...

    def push(self) -> None:
        ...

    def pop(self) -> None:
        ...

    def statistics(self) -> Any:
        ...

    def set(self, *options: Any) -> Any:
        ...

    def reason_unknown(self) -> Any:
        ...


class BoolRef:
    ...


def BVAddNoOverflow(a: Union[BitVec, int, BitVecVal], b: Union[BitVec, int, BitVecVal], signed: bool) -> BoolRef:
    ...


def BVAddNoUnderflow(a: Union[BitVec, int, BitVecVal], b: Union[BitVec, int, BitVecVal]) -> BoolRef:
    ...


def BVMulNoOverflow(a: Union[BitVec, int, BitVecVal], b: Union[BitVec, int, BitVecVal], signed: bool) -> BoolRef:
    ...


def BVMulNoUnderflow(a: Union[BitVec, int, BitVecVal], b: Union[BitVec, int, BitVecVal]) -> BoolRef:
    ...


def BVSubNoUnderflow(a: Union[BitVec, int, BitVecVal], b: Union[BitVec, int, BitVecVal], signed: bool) -> BoolRef:
    ...


def BVSubNoOverflow(a: Union[BitVec, int, BitVecVal], b: Union[BitVec, int, BitVecVal]) -> BoolRef:
    ...


def BVSDivNoOverflow(a: Union[BitVec, int, BitVecVal], b: Union[BitVec, int, BitVecVal]) -> BoolRef:
    ...


def Concat(*arg: Union[int, BitVec, BitVecVal]) -> BitVec:
    ...


def Extract(high: int, low: int, b: Union[int, BitVec, BitVecVal]) -> BitVec:
    ...


sat: Any = ...

unsat: Any = ...

unknown: Any = ...

And: Any = ...

Or: Any = ...

Implies: Any = ...

Not: Any = ...
